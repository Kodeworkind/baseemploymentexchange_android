package in.goaemex.www.di;

import android.app.Application;

import javax.inject.Singleton;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;
import in.goaemex.www.GEXApplication;
import in.goaemex.www.data.module.DatabaseModule;
import in.goaemex.www.data.module.NetworkModule;
import in.goaemex.www.data.module.RepositoryModule;
import in.goaemex.www.data.module.RxBusModule;
import in.goaemex.www.data.module.SharedPrefModule;

/**
 * Created by Vaibhav Barad on 5/23/2018.
 */

/**
 * This is a Dagger component. Refer to {@link GEXApplication} for the list of Dagger components
 * used in this application.
 * <p>
 * Even though Dagger allows annotating a {@link Component} as a singleton, the code
 * itself must ensure only one instance of the class is created. This is done in {@link
 * GEXApplication}.
 * <p>
 * {@link AndroidSupportInjectionModule} is the module from Dagger.Android that helps with the generation
 * and location of subcomponents.
 */
@Singleton
@Component(modules = {
        RepositoryModule.class,
        ApplicationModule.class,
        NetworkModule.class,
        RxBusModule.class,
        DatabaseModule.class,
        SharedPrefModule.class,
        ActivityBindingModule.class,
        AndroidSupportInjectionModule.class})
public interface AppComponent extends AndroidInjector<GEXApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }

    void inject(GEXApplication app);
}