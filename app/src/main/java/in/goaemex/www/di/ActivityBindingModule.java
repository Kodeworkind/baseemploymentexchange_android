package in.goaemex.www.di;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import in.goaemex.www.SplashActivity;
import in.goaemex.www.dashboard.DashboardActivity;
import in.goaemex.www.dashboard.DashboardActivityModule;
import in.goaemex.www.dashboard.edit_profile.EditProfileActivity;
import in.goaemex.www.dashboard.edit_profile.EditProfileModule;
import in.goaemex.www.dashboard.employer_attendance.EmployerAttendanceModule;
import in.goaemex.www.dashboard.interview_status.InterviewStausModule;
import in.goaemex.www.dashboard.profile.ProfileModule;
import in.goaemex.www.dashboard.qr_scan.ScanModule;
import in.goaemex.www.dashboard.scan_log.ScanLogModule;
import in.goaemex.www.login.LoginActivity;
import in.goaemex.www.login.LoginModule;
import in.goaemex.www.recruiter_option.RecruiterOptionActivity;

/**
 * We want Dagger.Android to create a Subcomponent which has a parent Component of whichever module ActivityBindingModule is on,
 * in our case that will be AppComponent. The beautiful part about this setup is that you never need to tell AppComponent that it is going to have all these subcomponents
 * nor do you need to tell these subcomponents that AppComponent exists.
 * <p>
 * We are also telling Dagger.Android that this generated SubComponent needs to include the specified modules and be aware of a
 * scope annotation @ActivityScoped
 * When Dagger.Android annotation processor runs it will create 1 subcomponent for us.
 */
@Module
public abstract class ActivityBindingModule {

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract RecruiterOptionActivity recruiterOptionActivity();

    @ActivityScoped
    @ContributesAndroidInjector()
    abstract SplashActivity splashActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = {DashboardActivityModule.class, ScanModule.class, ScanLogModule.class, EmployerAttendanceModule.class, ProfileModule.class, InterviewStausModule.class})
    abstract DashboardActivity dashboardActivity();

    @ActivityScoped
    @ContributesAndroidInjector(modules = LoginModule.class)
    abstract LoginActivity loginActivity();


    @ActivityScoped
    @ContributesAndroidInjector(modules = EditProfileModule.class)
    abstract EditProfileActivity editProfileActivity();
}