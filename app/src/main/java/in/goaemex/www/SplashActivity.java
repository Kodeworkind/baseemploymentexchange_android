package in.goaemex.www;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;

import javax.inject.Inject;

import dagger.android.support.DaggerAppCompatActivity;
import in.goaemex.www.dashboard.DashboardActivity;
import in.goaemex.www.data.local.SharedPrefsHelper;
import in.goaemex.www.login.LoginActivity;

import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_KEY_LOGIN_STATUS;

public class SplashActivity extends DaggerAppCompatActivity {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @Inject
    SharedPrefsHelper mPrefsHelper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        new Handler().postDelayed(() -> {
            if (mPrefsHelper.get(PREF_KEY_LOGIN_STATUS, false))
                startActivity(new Intent(SplashActivity.this, DashboardActivity.class));
            else
                startActivity(new Intent(SplashActivity.this, LoginActivity.class));

            finish();
        }, 2500);
    }
}
