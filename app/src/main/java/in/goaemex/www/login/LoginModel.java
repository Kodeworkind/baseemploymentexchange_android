package in.goaemex.www.login;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public class LoginModel {
    @SerializedName("objectItems")
    @Expose
    public ArrayList<ObjectItem> objectItems = null;

    public ArrayList<ObjectItem> getObjectItems() {
        return objectItems;
    }

    public void setObjectItems(ArrayList<ObjectItem> objectItems) {
        this.objectItems = objectItems;
    }

    public static class ObjectItem {

        @SerializedName("status")
        @Expose
        public String status;
        @SerializedName("userid")
        @Expose
        public int userid;
        @SerializedName("userTypeID")
        @Expose
        public int userTypeID;
        @SerializedName("emailID")
        @Expose
        public String emailID;

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public int getUserTypeID() {
            return userTypeID;
        }

        public void setUserTypeID(int userTypeID) {
            this.userTypeID = userTypeID;
        }

        public String getEmailID() {
            return emailID;
        }

        public void setEmailID(String emailID) {
            this.emailID = emailID;
        }
    }

}
