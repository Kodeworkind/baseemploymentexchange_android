package in.goaemex.www.login;

import dagger.Binds;
import dagger.Module;
import in.goaemex.www.di.ActivityScoped;

/**
 * Created by Vaibhav Barad on 3/28/2018.
 */

@Module
abstract public class LoginModule {

    @ActivityScoped
    @Binds
    abstract LoginContract.Presenter loginPresenter(LoginPresenter presenter);

    @ActivityScoped
    @Binds
    abstract LoginContract.View loginView(LoginActivity activity);
}
