package in.goaemex.www.login;

import in.goaemex.www.base_class.BasePresenter;
import in.goaemex.www.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 3/28/2018.
 */

public class LoginContract {

    interface View extends BaseView<Presenter> {

        void performLogin();

        void showLoginSuccess(LoginModel userModel);

        void showError(String message, int inputField);

        void showLoginFailure(String message);
    }

    interface Presenter extends BasePresenter<View> {

        void loginBtnClick(String email, String Password);
    }
}
