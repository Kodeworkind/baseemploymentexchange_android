package in.goaemex.www.login;

import android.util.Log;
import android.util.Patterns;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vaibhav Barad on 3/28/2018.
 */

public class LoginPresenter implements LoginContract.Presenter {

    public static final String TAG = LoginPresenter.class.getSimpleName();
    private LoginContract.View mLoginView;
    private DataSource mDataSource;

    private CompositeDisposable compositeDisposable;
    private Disposable userDisposable;

    @Inject
    public LoginPresenter(LoginContract.View view, Repository mDataSource, CompositeDisposable compositeDisposable) {
        this.mDataSource = mDataSource;
        this.mLoginView = view;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void takeView(LoginContract.View view) {
        this.mLoginView = view;
    }

    @Override
    public void dropView() {
        mLoginView = null;
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
           // compositeDisposable.remove(userDisposable);
            compositeDisposable.clear();
        }
    }

    @Override
    public void loginBtnClick(String email, String password) {
        if (!email.equals("")) {
            if (!Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
                mLoginView.showError("Please enter a valid email", 1);
                return;
            }
        } else {
            mLoginView.showError("Please enter a valid email", 1);
            return;
        }
        if (password.equals("")) {
            mLoginView.showError("Please enter a valid email", 2);
            return;
        }

        mLoginView.performLogin();
        try {
            performLogin(email, password);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void performLogin(String email, String password) throws JSONException {
        DisposableSingleObserver<LoginModel> disposableSingleObserver = new DisposableSingleObserver<LoginModel>() {
            @Override
            public void onSuccess(LoginModel userModel) {
                if (userModel != null && userModel.getObjectItems().get(0).getStatus().equalsIgnoreCase("success"))
                    mLoginView.showLoginSuccess(userModel);
                else {
                    mLoginView.showLoginFailure("Please check out email and/or password");
                }
            }

            @Override
            public void onError(Throwable e) {
                mLoginView.showLoginFailure(e.getMessage());
            }
        };

        if (!compositeDisposable.isDisposed()) {
            Single<LoginModel> mUserSingle = mDataSource.performLogin(formattedJSON(email, password));
            userDisposable = mUserSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(disposableSingleObserver);
            compositeDisposable.add(disposableSingleObserver);
            compositeDisposable.add(userDisposable);
        }
    }

    private String formattedJSON(String email, String password) throws JSONException, NumberFormatException {
        JSONObject object = new JSONObject();
        JSONArray uploadDataList = new JSONArray();

        JSONObject uploadObject = new JSONObject().put("email", email)
                .put("password", password)
                .put("timestamp", String.valueOf(System.currentTimeMillis()));
        object.put("objectItems", uploadDataList.put(uploadObject));
        Log.e("TAG", object.toString());
        return object.toString();
    }
}
