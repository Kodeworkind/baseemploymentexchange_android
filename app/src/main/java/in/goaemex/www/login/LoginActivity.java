package in.goaemex.www.login;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import in.goaemex.www.R;
import in.goaemex.www.dashboard.DashboardActivity;
import in.goaemex.www.data.local.SharedPrefsHelper;
import in.goaemex.www.utility.CircularAnimatedDrawable.CircularProgressButton;
import in.goaemex.www.utility.CircularAnimatedDrawable.Constant;
import in.goaemex.www.utility.CustomProgressDialog;

import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_KEY_LOGIN_STATUS;
import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_EMAIL;
import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_ID;
import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_ROLE;
import static in.goaemex.www.utility.CircularAnimatedDrawable.Constant.mStatus;


/**
 * Created by Vaibhav Barad on 3/28/2018.
 */

public class LoginActivity extends DaggerAppCompatActivity implements LoginContract.View {


    @Inject
    LoginContract.Presenter mLoginPresenter;
    @Inject
    SharedPrefsHelper mPrefsHelper;
    @BindView(R.id.iv_man)
    ImageView ivMan;
    @BindView(R.id.email)
    EditText email;
    @BindView(R.id.password)
    EditText password;
    @BindView(R.id.tv_error_text)
    TextView tvErrorText;
    @BindView(R.id.btn_post)
    CircularProgressButton btnLogin;
    @BindView(R.id.constraintLayout)
    ConstraintLayout constraintLayout;
    boolean isDismissing = false, isPosting = false, isAnimating = false;
    @BindView(R.id.usernameWrapper)
    TextInputLayout usernameWrapper;
    @BindView(R.id.passwordWrapper)
    TextInputLayout passwordWrapper;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.btn_post)
    public void OnLoginClick(View view) {
        mLoginPresenter.loginBtnClick(email.getText().toString(), password.getText().toString());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mLoginPresenter.takeView(this);
    }


    @Override
    protected void onStop() {
        super.onStop();
        mLoginPresenter.dropView();
    }

    @Override
    public void performLogin() {
        showProgressLoader(true);
        usernameWrapper.setErrorEnabled(false);
        passwordWrapper.setErrorEnabled(false);

        isPosting = true;
        btnLogin.startAnimation();
        tvErrorText.setVisibility(View.GONE);
        new Handler().postDelayed(() -> checkAndDismissLoader(), 0);
        isAnimating = true;

    }

    @Override
    public void showError(String message, int inputField) {
        switch (inputField) {
            case 1:
                usernameWrapper.setError(message);
                break;
            case 2:
                passwordWrapper.setError(message);
                break;
            default:
                Toast.makeText(LoginActivity.this, message, Toast.LENGTH_SHORT).show();
        }

    }

    @Override
    public void showLoginSuccess(LoginModel userModel) {
        showProgressLoader(false);
        Toast.makeText(LoginActivity.this, "Authentication successful.",
                Toast.LENGTH_SHORT).show();

        mPrefsHelper.put(PREF_KEY_LOGIN_STATUS, true);
        mPrefsHelper.put(PREF_USER_ID, userModel.getObjectItems().get(0).getUserid());
        mPrefsHelper.put(PREF_USER_ROLE, userModel.getObjectItems().get(0).getUserTypeID());
        mPrefsHelper.put(PREF_USER_EMAIL, userModel.getObjectItems().get(0).getEmailID());

        mStatus = Constant.CircularButtonStatus.SUCCESSFUL;
        if (!isAnimating)
            checkAndDismissLoader();
    }

    @Override
    public void showLoginFailure(String message) {
        showProgressLoader(false);
        mStatus = Constant.CircularButtonStatus.FAILED;

        if (!isAnimating)
            checkAndDismissLoader();

        Toast.makeText(LoginActivity.this, message,
                Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressLoader(boolean toShow) {
        if (toShow) {
            CustomProgressDialog.showProgressDialog(this, null);
        } else {
            CustomProgressDialog.hideProgressDialog();
        }
    }

    void checkAndDismissLoader() {
        isAnimating = false;
        isPosting = false;

        if (mStatus == Constant.CircularButtonStatus.IDLE) {
            return;
        }

        if (mStatus == Constant.CircularButtonStatus.SUCCESSFUL) {
            btnLogin.doneLoadingAnimation(
                    ContextCompat.getColor(this, R.color.tick_background),
                    BitmapFactory.decodeResource(getResources(), R.drawable.ic_check_black_24dp));
            new Handler().postDelayed(() -> {
                dismiss();
                isPosting = false;
            }, 1000);
        } else {
            btnLogin.revertAnimation(() -> {
                btnLogin.setText(getString(R.string.try_again));
                isPosting = false;
                Toast.makeText(LoginActivity.this, "Invalid credential", Toast.LENGTH_LONG).show();
            });
        }
    }

    private void dismiss() {
        isDismissing = true;
        if (mStatus == Constant.CircularButtonStatus.SUCCESSFUL) {
            startActivity(new Intent(LoginActivity.this, DashboardActivity.class));
            finish();
        }

    }
}
