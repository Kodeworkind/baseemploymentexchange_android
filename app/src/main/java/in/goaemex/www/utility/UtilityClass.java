package in.goaemex.www.utility;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;

import java.text.DateFormat;
import java.util.Date;
import java.util.TimeZone;

import in.goaemex.www.data.local.SharedPrefsHelper;

import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_ROLE;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public class UtilityClass {
    public static final String CAMERA_START = "PERMISSION_GRANTED";
    public static final int PERMISSION_CALLBACK_CONSTANT = 100;

    public static final String[] PERMISSIONS = {Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE};

    public static UserRole mRoleType;

    public static String getUserRole(int roleID) {

        return null;
    }

    public static String getInterviewStatus(int statusID) {
        switch (statusID) {
            case 1:
                return "Shortlisted";
            case 2:
                return "Hired";
            case 3:
                return "Not interested";
            case 4:
                return "Pending Scrutiny";
            default:
                return "";
        }

    }

    /*Check if the necessary permissions were granted and return boolean */
    public static boolean hasPermissions(Context context, String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions != null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    public static String getUTCTime() {
        DateFormat df = DateFormat.getTimeInstance();
        df.setTimeZone(TimeZone.getTimeZone("UTC"));
        return df.format(new Date().getTime());
    }

    public static void defineUserRole(SharedPrefsHelper mPrefsHelper) {
        int userRole = mPrefsHelper.get(PREF_USER_ROLE, 1);

        mRoleType = UtilityClass.UserRole.ORG;
        if (userRole == 2) {
            mRoleType = UtilityClass.UserRole.USR;
        } else if (userRole == 3) {
            mRoleType = UtilityClass.UserRole.REC;
        }
    }

    public static String escapeString(String dataStr) {
        dataStr = dataStr.replace("\"", "\"\"");
        if (dataStr.contains(","))
            dataStr = "\"" + dataStr + "\"";
        return dataStr;
    }

    public enum UserRole {
        USR, ORG, REC
    }


}
