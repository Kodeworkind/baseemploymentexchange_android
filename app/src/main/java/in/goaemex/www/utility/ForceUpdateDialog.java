package in.goaemex.www.utility;

import android.app.Dialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import in.goaemex.www.BuildConfig;
import in.goaemex.www.R;
import in.goaemex.www.dashboard.DashboardActivity;
import in.goaemex.www.data.local.DatabaseHelper;

/**
 * Created by Vaibhav Barad on 2/3/2018.
 */

public class ForceUpdateDialog extends Dialog implements View.OnClickListener {

    private DashboardActivity mContext;
    private boolean forceUpdate, packageIDChanged;
    private String updateLink;
    private TextView update;
    SharedPreferences preferences;

    DatabaseHelper databaseHelper;

    public ForceUpdateDialog(DashboardActivity mContext, boolean forceUpdate, String updateLink, boolean packageIDChanged) {
        super(mContext);
        this.mContext = mContext;
        this.forceUpdate = forceUpdate;
        this.updateLink = updateLink;
        this.packageIDChanged = packageIDChanged;
        preferences = mContext.getSharedPreferences(BuildConfig.APP_PREFS, Context.MODE_PRIVATE);
        databaseHelper = new DatabaseHelper(mContext);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.update_app);
        TextView title = (TextView) findViewById(R.id.title);
        TextView descriptionForce = (TextView) findViewById(R.id.descriptionForce);
        update = (TextView) findViewById(R.id.update);
        update.setOnClickListener(this);
        if (forceUpdate) {
            title.setText("Update Required");
            descriptionForce.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.update:
                if (packageIDChanged) {
                    mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                            (updateLink)));
                } else {
                    try {
                        mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                                ("market://details?id=in.goaemex.www")));
                    } catch (ActivityNotFoundException anfe) {
                        mContext.startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                                (updateLink)));
                    }
                }

                preferences.edit().clear().apply();
                databaseHelper.delete();
                mContext.finish();
                break;
        }
        dismiss();
    }
}
