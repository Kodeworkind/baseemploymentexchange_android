package in.goaemex.www.qualification_search;

/**
 * Created by Vaibhav Barad on 5/27/2018.
 */
public class QualificationModel {
    String name;
    boolean isHeader;

    public QualificationModel(String name, boolean isHeader) {
        this.name = name;
        this.isHeader = isHeader;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isHeader() {
        return isHeader;
    }

    public void setHeader(boolean header) {
        isHeader = header;
    }

    @Override
    public boolean equals(Object o) {

        if (o == this) return true;
        if (!(o instanceof QualificationModel)) {
            return false;
        }

        QualificationModel user = (QualificationModel) o;

        return user.name.equals(name) &&
                user.isHeader == isHeader;
    }

    @Override
    public int hashCode() {
        int result = 17;
        result = 31 * result + name.hashCode();
        result = 31 * result + (isHeader?10:11);
        return result;
    }
}
