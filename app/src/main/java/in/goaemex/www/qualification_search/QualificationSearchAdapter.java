package in.goaemex.www.qualification_search;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.goaemex.www.R;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
class QualificationSearchAdapter extends RecyclerView.Adapter implements Filterable {

    QualificationCallback listener;
    int HEADER = 1, ITEM = 2;

    private ArrayList<QualificationModel> qualificationList;
    private ArrayList<QualificationModel> mFilteredList;

    public QualificationSearchAdapter(QualificationCallback listener, ArrayList<QualificationModel> qualificationList) {
        this.listener = listener;
        this.qualificationList = qualificationList;
        mFilteredList = qualificationList;
    }

    @Override
    public int getItemViewType(int position) {
        return mFilteredList.get(position).isHeader() ? HEADER : ITEM;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder holder;
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        if (viewType == HEADER) {
            holder = new HeaderViewHolder(layoutInflater.inflate(R.layout.search_header, parent, false));
        } else {
            holder = new ItemViewHolder(layoutInflater.inflate(R.layout.search_item, parent, false));
        }
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) holder).section.setText(mFilteredList.get(position).getName());
        } else {
            ((ItemViewHolder) holder).child.setText(mFilteredList.get(position).getName());
        }

    }

    @Override
    public int getItemCount() {
        return mFilteredList.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                String charString = constraint.toString();

                if (charString.isEmpty()) {
                    mFilteredList = qualificationList;
                } else {
                    ArrayList<QualificationModel> filteredList = new ArrayList<>();
                    boolean isNewHeader = false;
                    QualificationModel sectionTmp = null;
                    for (QualificationModel model : qualificationList) {
                        if (model.isHeader()) {
                            isNewHeader = true;
                            sectionTmp = model;
                        } else {
                            if (model.getName().toLowerCase().contains(charString.toLowerCase())) {
                                if (isNewHeader) {
                                    filteredList.add(sectionTmp);
                                    isNewHeader = false;
                                }
                                filteredList.add(model);
                            }

                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                mFilteredList = (ArrayList<QualificationModel>) results.values;
                notifyDataSetChanged();
            }
        };
    }

    interface QualificationCallback {
        void OnClickListener(int position, String value);
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.section)
        TextView section;

        public HeaderViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.child)
        TextView child;

        public ItemViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(v -> listener.OnClickListener(getAdapterPosition(), mFilteredList.get(getAdapterPosition()).getName()));
        }
    }
}
