package in.goaemex.www.qualification_search;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.goaemex.www.R;

/**
 * Created by Vaibhav Barad on 5/27/2018.
 */
public class QualificationSearchActivity extends AppCompatActivity implements QualificationSearchAdapter.QualificationCallback {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    ArrayList<QualificationModel> qualificationList;
    @BindView(R.id.toolbar2)
    Toolbar toolbar;
    @BindView(R.id.recyclerview)
    RecyclerView recyclerview;
    @BindView(R.id.search)
    SearchView search;

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.qualification_search_activity);
        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        formatData();

        search.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                ((QualificationSearchAdapter) recyclerview.getAdapter()).getFilter().filter(newText);
                return false;
            }
        });

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        recyclerview.setAdapter(new QualificationSearchAdapter(this, qualificationList));
    }

    private void formatData() {
        qualificationList = new ArrayList<>();
        qualificationList.add(new QualificationModel("", true));
        qualificationList.add(new QualificationModel("Below - SSC", false));
        qualificationList.add(new QualificationModel("10TH/MATRIC/SSLC PASS", false));


        qualificationList.add(new QualificationModel("12th", true));
        qualificationList.add(new QualificationModel("12TH Arts", false));
        qualificationList.add(new QualificationModel("12th Science", false));
        qualificationList.add(new QualificationModel("12th Commerce", false));
        qualificationList.add(new QualificationModel("12th Vocational", false));


        qualificationList.add(new QualificationModel("OTHERS", true));
        qualificationList.add(new QualificationModel("ILITERATE", false));


        qualificationList.add(new QualificationModel("DIPLOMA", true));
        qualificationList.add(new QualificationModel("DIPLOMA IN PERSONNEL MANAGEMENT", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN OFFICE MANAGEMENT", false));
        qualificationList.add(new QualificationModel("DIPLOMA - S.C.V.T", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN FOOD PRODUCTION", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN HOTEL MANAGEMENT", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN AYURVEDIC MEDICINE", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN ARTS", false));
        qualificationList.add(new QualificationModel("DIPLOMA BUSINESS MGMT.", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN COMMERCE", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN EDUCATION", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN ENGINEERING", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN FINE ARTS", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN AGRICULTURE", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN HOMESCIENCE", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN LIBRARY SCIENCE", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN COMPUTER APPLICATIONS", false));
        qualificationList.add(new QualificationModel("POST DIPLOMA", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN LAW", false));
        qualificationList.add(new QualificationModel("DIPLOMA-MEDICAL TRADES", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN NURSING", false));
        qualificationList.add(new QualificationModel("DIPLOMA PHARMACY", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN ARCHITECTURE", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN VETERINARY", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN SOCIAL WORK", false));
        qualificationList.add(new QualificationModel("DIPLOMA IN MUSIC", false));
        qualificationList.add(new QualificationModel("D.PH.EDUCATION", false));
        qualificationList.add(new QualificationModel("DIPLOMA-OTHERS", false));
        qualificationList.add(new QualificationModel("Diploma In Mechanical Engineering", false));
        qualificationList.add(new QualificationModel("Diploma in Electrical Engineering", false));
        qualificationList.add(new QualificationModel("Diploma in Electronic Engineering", false));
        qualificationList.add(new QualificationModel("Diploma in Mining", false));
        qualificationList.add(new QualificationModel("Diploma in Fabrication", false));
        qualificationList.add(new QualificationModel("Diploma in Civil Engineering", false));


        qualificationList.add(new QualificationModel("ITI", true));
        qualificationList.add(new QualificationModel("Computer Operator and Programming Assistant", false));
        qualificationList.add(new QualificationModel("Information Technology & Electronic System Maintenance", false));
        qualificationList.add(new QualificationModel("Desk Top Publishing Operator", false));
        qualificationList.add(new QualificationModel("Fitter", false));
        qualificationList.add(new QualificationModel("Turner", false));
        qualificationList.add(new QualificationModel("Machinist", false));
        qualificationList.add(new QualificationModel("Electrician", false));
        qualificationList.add(new QualificationModel("Instrument Mechanic", false));
        qualificationList.add(new QualificationModel("Mech. Refrigeration and Air Conditioning", false));
        qualificationList.add(new QualificationModel("Electronic Mechanic", false));
        qualificationList.add(new QualificationModel("Draughtsman Mechanical", false));
        qualificationList.add(new QualificationModel("Draughtsman (Civil)", false));
        qualificationList.add(new QualificationModel("Stenography (English)", false));
        qualificationList.add(new QualificationModel("Secretarial Practice", false));
        qualificationList.add(new QualificationModel("Mechanic Motor Vehicle", false));
        qualificationList.add(new QualificationModel("Hair & Skin Care", false));
        qualificationList.add(new QualificationModel("Craftsman Food Production (General)", false));
        qualificationList.add(new QualificationModel("Steward", false));
        qualificationList.add(new QualificationModel("Mechanic Diesel", false));
        qualificationList.add(new QualificationModel("Welder (Gas and Electric)", false));
        qualificationList.add(new QualificationModel("Carpenter", false));
        qualificationList.add(new QualificationModel("Wireman", false));
        qualificationList.add(new QualificationModel("Plumber", false));
        qualificationList.add(new QualificationModel("Cutting & Sewing", false));
        qualificationList.add(new QualificationModel("Hospitality Management-Broad Based Basic Training", false));
        qualificationList.add(new QualificationModel("Hospitality Management- (Food Production)", false));
        qualificationList.add(new QualificationModel("Hospitality Management- (Front Office Management)", false));
        qualificationList.add(new QualificationModel("Hospitality Management- (Food & Beverage Service)", false));
        qualificationList.add(new QualificationModel("Hospitality Management- (House Keeping)", false));
        qualificationList.add(new QualificationModel("Production and Manufacturing-Broad Based Basic Training", false));
        qualificationList.add(new QualificationModel("Production and Manufacturing- (CNC Machining)", false));
        qualificationList.add(new QualificationModel("Production and Manufacturing- (PLC & Automation)", false));
        qualificationList.add(new QualificationModel("Information Technology- Broad Based Basic Training", false));
        qualificationList.add(new QualificationModel("Information Technology- (Hardware & Maintenance)", false));
        qualificationList.add(new QualificationModel("Information Technology- ( Multimedia & Web Designing)", false));
        qualificationList.add(new QualificationModel("Information Technology- (Computer Networking)", false));
        qualificationList.add(new QualificationModel("Fabrication (Fitting & Welding)-Broad Based Basic Training", false));
        qualificationList.add(new QualificationModel("Fabrication (Fitting & Welding)- (TIG/MIG Welding)", false));
        qualificationList.add(new QualificationModel("Fabrication (Fitting & Welding)- (Pressure Vessel & Pipe Welding)", false));
        qualificationList.add(new QualificationModel("Fabrication (Fitting & Welding)- (Welding Inspection & Testing)", false));
        qualificationList.add(new QualificationModel("Automobile-Broad Based Basic Training", false));
        qualificationList.add(new QualificationModel("Automobile- (Petrol)", false));
        qualificationList.add(new QualificationModel("Automobile- (Diesel)", false));
        qualificationList.add(new QualificationModel("Automobile- (Auto Elect.  Electronics & A/C in Automobiles)", false));
        qualificationList.add(new QualificationModel("Automobile- (Overhauling of Fuel Injection System & Steering Mechanism)", false));
        qualificationList.add(new QualificationModel("Automobile-Advanced Modules (Denting, Painting & Welding)", false));
        qualificationList.add(new QualificationModel("Electrical-Broad Based Basic Training", false));
        qualificationList.add(new QualificationModel("Electrical- (Repair & Maintenance of Domestic Appliances)", false));
        qualificationList.add(new QualificationModel("Electrical- (Operation & Maintenance of Equipment�s used in HT, LT, Sub-station & Cable Jointing)", false));
        qualificationList.add(new QualificationModel("Electrical- (Repair & Maintenance of Electrical Machine & Power Supply)", false));
        qualificationList.add(new QualificationModel("Data Entry Operator (Short Term)", false));
        qualificationList.add(new QualificationModel("Driver cum Mechanic-Light Motor Vehicle (Short Term)", false));


        qualificationList.add(new QualificationModel("POST GRADUATE DIPLOMA", true));
        qualificationList.add(new QualificationModel("POST GRADUATE DIPLOMA IN COMPUTER APPL.", false));
        qualificationList.add(new QualificationModel("POST GR.DIPLOMA IN CLINICAL GENETICS & MED.LB", false));
        qualificationList.add(new QualificationModel("POST GR. DIPLOMA IN URBAN & REGIONAL PLANING", false));
        qualificationList.add(new QualificationModel("PGDCET - (P.G. DIP IN COMPUTER EDU.TECH)", false));
        qualificationList.add(new QualificationModel("POST DIPLOMA CERTIFICATE � CRAFT INSTRUCTOR", false));


        qualificationList.add(new QualificationModel("BACHELOR", true));
        qualificationList.add(new QualificationModel("BACHELOR  IN PHYSIOTHERAPHY", false));
        qualificationList.add(new QualificationModel("BACHELOR IN FISHERY SCIENCE", false));
        qualificationList.add(new QualificationModel("BACHELOR IN AYURVEDIC", false));
        qualificationList.add(new QualificationModel("B.SC", false));
        qualificationList.add(new QualificationModel("B.SC (HORTICULTURE)", false));
        qualificationList.add(new QualificationModel("B.A.", false));
        qualificationList.add(new QualificationModel("BACHELOR IN BUSINESS MANAGEMENT", false));
        qualificationList.add(new QualificationModel("B.COM", false));
        qualificationList.add(new QualificationModel("B.ED.", false));
        qualificationList.add(new QualificationModel("B.E.", false));
        qualificationList.add(new QualificationModel("BACHELOR IN FINE ARTS", false));
        qualificationList.add(new QualificationModel("B.AGRI", false));
        qualificationList.add(new QualificationModel("BSC(HOME SC)", false));
        qualificationList.add(new QualificationModel("BACHELOR IN LIB. SC", false));
        qualificationList.add(new QualificationModel("BACHELOR IN COMPUTER APPLICATIONS", false));
        qualificationList.add(new QualificationModel("BCA (COMMERCE)", false));
        qualificationList.add(new QualificationModel("LLB", false));
        qualificationList.add(new QualificationModel("MBBS", false));
        qualificationList.add(new QualificationModel("BSC(NURSING)", false));
        qualificationList.add(new QualificationModel("BACHELOR IN HOMEOPATHY", false));
        qualificationList.add(new QualificationModel("B PHARMACY", false));
        qualificationList.add(new QualificationModel("B.D.S.", false));
        qualificationList.add(new QualificationModel("B.ARCH", false));
        qualificationList.add(new QualificationModel("B.TECH", false));
        qualificationList.add(new QualificationModel("B.VSC", false));
        qualificationList.add(new QualificationModel("BACHELOR IN SOCIAL WORK", false));
        qualificationList.add(new QualificationModel("B.A. IN MUSIC", false));
        qualificationList.add(new QualificationModel("B.PH.ED", false));
        qualificationList.add(new QualificationModel("BACHELOR IN BUSINESS STUDIES", false));
        qualificationList.add(new QualificationModel("BACHELOR IN FINANCIAL SERVICES", false));
        qualificationList.add(new QualificationModel("BACHELOR IN FASHION DESIGNING", false));
        qualificationList.add(new QualificationModel("BACHELOR IN HOSPITALITY & CATERING", false));
        qualificationList.add(new QualificationModel("BACHELOR IN JOURNALISM", false));
        qualificationList.add(new QualificationModel("BACHELOR IN LABOUR LAW", false));
        qualificationList.add(new QualificationModel("B.SC (M.T.L.)", false));
        qualificationList.add(new QualificationModel("BACHELOR IN HOSPITAL MANAGEMENT", false));
        qualificationList.add(new QualificationModel("BACHELOR IN FORIEGN TRADE", false));
        qualificationList.add(new QualificationModel("BACHELOR IN FORENSIC SCIENCE", false));
        qualificationList.add(new QualificationModel("BACHELOR IN TRAVEL & TOURISM", false));
        qualificationList.add(new QualificationModel("BACHELOR IN SPEECH THERAPY & AUDIOLOGY", false));
        qualificationList.add(new QualificationModel("BACHELOR IN NATUREPATHY & YOGIC SCIENCE", false));
        qualificationList.add(new QualificationModel("Bachelor in Mechanical Engineering", false));
        qualificationList.add(new QualificationModel("Bachelor  in Electrical Engineering", false));
        qualificationList.add(new QualificationModel("Bachelor in Electronic Engineering", false));
        qualificationList.add(new QualificationModel("Bachelor in Mining", false));
        qualificationList.add(new QualificationModel("Bachelor in Fabrication", false));
        qualificationList.add(new QualificationModel("Bachelor in Civil Engineering", false));


        qualificationList.add(new QualificationModel("MASTERS DEGREE", true));
        qualificationList.add(new QualificationModel("M.A.", false));
        qualificationList.add(new QualificationModel("MBA/MPM", false));
        qualificationList.add(new QualificationModel("M.COM", false));
        qualificationList.add(new QualificationModel("M.ED.", false));
        qualificationList.add(new QualificationModel("M.E.", false));
        qualificationList.add(new QualificationModel("MASTER IN FINE ARTS", false));
        qualificationList.add(new QualificationModel("M.AGRI", false));
        qualificationList.add(new QualificationModel("MSC(HOME SC.", false));
        qualificationList.add(new QualificationModel("MASTER DEGREE IN LIBRARY SCIENCE", false));
        qualificationList.add(new QualificationModel("M.C.A.", false));
        qualificationList.add(new QualificationModel("PCA (COMMERCE)", false));
        qualificationList.add(new QualificationModel("LLM", false));
        qualificationList.add(new QualificationModel("MS/MD", false));
        qualificationList.add(new QualificationModel("MSC(NURSING)", false));
        qualificationList.add(new QualificationModel("MASTER IN HOMEOPATHY", false));
        qualificationList.add(new QualificationModel("M.PHARMACY", false));
        qualificationList.add(new QualificationModel("M.D.S.", false));
        qualificationList.add(new QualificationModel("M.ARCH", false));
        qualificationList.add(new QualificationModel("M.SC.", false));
        qualificationList.add(new QualificationModel("M. TECH", false));
        qualificationList.add(new QualificationModel("CHARTERED ACCOUNTANT", false));
        qualificationList.add(new QualificationModel("M.VSC.", false));
        qualificationList.add(new QualificationModel("MASTER IN SOCIAL WORK", false));
        qualificationList.add(new QualificationModel("M.A. IN MUSIC", false));
        qualificationList.add(new QualificationModel("M.PED.", false));
        qualificationList.add(new QualificationModel("COMPANY SECRETARYSHIP", false));
        qualificationList.add(new QualificationModel("MASTER IN BUSINESS STUDIES", false));
        qualificationList.add(new QualificationModel("MASTER IN FINANCIAL SERVICES", false));
        qualificationList.add(new QualificationModel("MASTER IN FASHION DESIGNING", false));
        qualificationList.add(new QualificationModel("M.SC. (HOSPITALITY & CATERING)", false));
        qualificationList.add(new QualificationModel("MASTER IN JOURNALISM", false));
        qualificationList.add(new QualificationModel("MASTER IN LABOUR LAW", false));
        qualificationList.add(new QualificationModel("MASTER IN BUSINESS LAW", false));
        qualificationList.add(new QualificationModel("MASTER OF HOSPITAL MANGEMENT", false));
        qualificationList.add(new QualificationModel("MASTER IN PHYSIOTHERAPY", false));
        qualificationList.add(new QualificationModel("MASTER IN FORENSIC SCIENCE", false));
        qualificationList.add(new QualificationModel("MASTER IN TRAVEL & TOURISM", false));
        qualificationList.add(new QualificationModel("MASTER IN SPEECH THERAPY & AUDIOLOGY", false));
        qualificationList.add(new QualificationModel("MASTER IN NATUREPATHY & YOGIC SCIENCE", false));
        qualificationList.add(new QualificationModel("MASTER IN PERSONNEL MANAGEMENT", false));
        qualificationList.add(new QualificationModel("MASTER IN MGMT STUDIES", false));
        qualificationList.add(new QualificationModel("MASTER IN FISHERY SCIENCE", false));
        qualificationList.add(new QualificationModel("MASTER IN URBAN & TOWN PLANING", false));
        qualificationList.add(new QualificationModel("MASTER IN AYURVEDIC", false));
        qualificationList.add(new QualificationModel("M.SC.(HORTICULTURE)", false));


        qualificationList.add(new QualificationModel("Master in Mechanical Engineering", false));
        qualificationList.add(new QualificationModel("Master in Electrical Engineering", false));
        qualificationList.add(new QualificationModel("Master in Electronic Engineering", false));
        qualificationList.add(new QualificationModel("Master  in Mining", false));
        qualificationList.add(new QualificationModel("Master  in Fabrication", false));
        qualificationList.add(new QualificationModel("Master  in Civil Engineering", false));


        qualificationList.add(new QualificationModel("M.PHIL", true));
        qualificationList.add(new QualificationModel("M.PHIL  IN ARTS", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN MAN./BUS. ADM.", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN COMMERCE", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN EDUCATION", false));
        qualificationList.add(new QualificationModel("M.PHIL   IN ENGINEERING", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN ARGICULTURE", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN HOME SCI.", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN LAW", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN MEDICAL", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN NURSING", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN PHARMACY", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN ARCHITECTURE", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN SCIENCE", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN VETERINARY", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN PHY. EDUCATION", false));
        qualificationList.add(new QualificationModel("M.PHIL  IN OTHERS", false));


        qualificationList.add(new QualificationModel("PHD", true));
        qualificationList.add(new QualificationModel("DOCTORATE IN ARTS", false));
        qualificationList.add(new QualificationModel("DOCT. OF MANG./BUS.ADMIN.", false));
        qualificationList.add(new QualificationModel("DOCTORATE OF COMMERCE", false));
        qualificationList.add(new QualificationModel("DOCTORATE OF EDUCATION", false));
        qualificationList.add(new QualificationModel("DOCTORATE IN ENGINEERING", false));
        qualificationList.add(new QualificationModel("DOCTRATE IN AGRICULTURE", false));
        qualificationList.add(new QualificationModel("DOCT. IN HOME SCIENCE.", false));
        qualificationList.add(new QualificationModel("DOCTORATE OF LAW", false));
        qualificationList.add(new QualificationModel("DOCTRATE IN MEDICAL", false));
        qualificationList.add(new QualificationModel("DOCTORATE IN NURSING", false));
        qualificationList.add(new QualificationModel("DOCTRATE IN PHARMACY", false));
        qualificationList.add(new QualificationModel("DOCT. IN ARCHITECTURE", false));
        qualificationList.add(new QualificationModel("DOCTORATE IN SCIENCE", false));
        qualificationList.add(new QualificationModel("DOCTRATE IN VETERINARY", false));
        qualificationList.add(new QualificationModel("DOCT. IN PHY. EDUCATION", false));
        qualificationList.add(new QualificationModel("DOCTORATE IN OTHERS", false));
    }

    @Override
    public void OnClickListener(int position, String value) {
        setResult(RESULT_OK, new Intent().putExtra("qualification", value));
        finish();
    }
}
