package in.goaemex.www.data.remote;

import android.accounts.NetworkErrorException;
import android.support.annotation.NonNull;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import javax.inject.Inject;
import javax.inject.Singleton;

import in.goaemex.www.dashboard.VersionValidity;
import in.goaemex.www.dashboard.edit_profile.EditProfileModel;
import in.goaemex.www.dashboard.employer_attendance.EmployerAttendanceModel;
import in.goaemex.www.dashboard.interview_status.InterviewStatusModel;
import in.goaemex.www.dashboard.profile.ProfileModel;
import in.goaemex.www.dashboard.qr_scan.QRUploadModel;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.data.DataSource;
import in.goaemex.www.login.LoginModel;
import io.reactivex.Single;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Retrofit;

import static in.goaemex.www.data.remote.ServiceError.ERROR_UNDEFINED;
import static in.goaemex.www.data.remote.ServiceError.NETWORK_ERROR;
import static in.goaemex.www.data.remote.ServiceError.SUCCESS_CODE;
import static in.goaemex.www.utility.ObjectUtil.isNull;

/**
 * Implementation of the DataSource as Retrofit(Network Calls).
 * Reference : https://github.com/ahmedeltaher/MVP-RX-Android-Sample
 */
@Singleton
public class RemoteRepo implements DataSource {

    public static final String TAG = RemoteRepo.class.getSimpleName();

    Retrofit mRetrofit;
    private RetrofitAPI retrofitAPI;

    @Inject
    public RemoteRepo(Retrofit retrofit, RetrofitAPI api) {
        this.mRetrofit = retrofit;
        this.retrofitAPI = api;
    }

    public static RequestBody JSONToRetrofitJSON(String JSONRequest) throws JSONException {
        return RequestBody.create(MediaType.parse("application/json; charset=utf-8"), (new JSONObject(JSONRequest)).toString());
    }

    @Override
    public Single<LoginModel> performLogin(String requestBody) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.loginUser(JSONToRetrofitJSON(requestBody)), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((LoginModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @Override
    public Single<LogModel> getScanLog(String requestBody) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.getScanLog(requestBody), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((LogModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @Override
    public Single<EmployerAttendanceModel> getEmployerLog(String userID) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.getCompanyData(userID), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((EmployerAttendanceModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @Override
    public Single<QRUploadModel> sendScanData(String requestBody) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.uploadQRData(JSONToRetrofitJSON(requestBody)), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((QRUploadModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @Override
    public boolean saveFailedEntry(String data) {
        return false;
    }

    @Override
    public String getFailedData() {
        return null;
    }

    @Override
    public void checkUpdateDatabase(QRUploadModel qrUploadModel) {
    }

    @Override
    public Single<ProfileModel> getProfileData(String formattedObject) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.getProfileData(JSONToRetrofitJSON(formattedObject)), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((ProfileModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @Override
    public Single<EditProfileModel> updateProfile(String formattedObject) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.updateProfile(JSONToRetrofitJSON(formattedObject)), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((EditProfileModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @Override
    public Single<InterviewStatusModel> getInterviewStatusLog(String userID) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.getInterviewStatus(userID), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((InterviewStatusModel) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @Override
    public Single<VersionValidity> checkVersionValidity(int versionCode) {
        return Single.create(emitter -> {
            ServiceResponse response = processCall(retrofitAPI.checkVersionValidity(versionCode), false);
            if (response.getCode() == SUCCESS_CODE) {
                emitter.onSuccess((VersionValidity) response.getData());
            } else {
                Throwable throwable = new NetworkErrorException();
                emitter.onError(throwable);
            }
        });
    }

    @NonNull
    private ServiceResponse processCall(Call call, boolean isVoid) {
        try {
            retrofit2.Response response = call.execute();
            if (isNull(response)) {
                return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
            }
            int responseCode = response.code();
            if (response.isSuccessful()) {
                return new ServiceResponse(responseCode, isVoid ? null : response.body());
            } else {
                ServiceError ServiceError;
                ServiceError = new ServiceError(response.message(), responseCode);
                return new ServiceResponse(ServiceError);
            }
        } catch (IOException e) {
            return new ServiceResponse(new ServiceError(NETWORK_ERROR, ERROR_UNDEFINED));
        }
    }
}