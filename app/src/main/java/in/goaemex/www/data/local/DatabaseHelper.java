package in.goaemex.www.data.local;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import in.goaemex.www.dashboard.qr_scan.QRUploadModel;


/**
 * Created by Vaibhav Barad on 1/25/2018.
 */

public class DatabaseHelper {

    public static final String DATABASE_NAME = "goaempex";
    public static final int DATABASE_VERSION = 2;
    private static final String KEY_FAILED_DATA = "failed_data";
    public static String DB_PATH;
    private final String DEBUG_TAG = "GoaEmpEx App";
    private final Context ourContext;
    SQLiteDatabase ourDb;
    private DBHelper ourDBHelper;
    private SQLiteDatabase ourDatabase;
    private String TABLE_NAME = "offline_table";


    public DatabaseHelper(Context c) {
        ourContext = c;
        DB_PATH = ourContext.getDatabasePath(DATABASE_NAME).getAbsolutePath();
        try {
            DBHelper helper = new DBHelper(ourContext);
            helper.createDataBase();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public DatabaseHelper open() {
        ourDBHelper = new DBHelper(ourContext);
        ourDatabase = ourDBHelper.getWritableDatabase();
        ourDb = ourDBHelper.getWritableDatabase();
        return this;
    }

    public void close() {
        ourDBHelper.close();
    }

    public boolean saveFailedEntry(String uploadRequest) {
        open();
        String insertQuery = "insert into " + TABLE_NAME + " ("
                + KEY_FAILED_DATA + ")" + "values ('"
                + uploadRequest + "')";
        Log.e(DEBUG_TAG, "Insert QUERY =>>" + insertQuery);
        try {
            ourDatabase.execSQL(insertQuery);
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        close();
        return true;
    }

    public String getAFailedEntry() {
        StringBuilder result = null;
        open();

        String grpQuery = "SELECT * FROM " + TABLE_NAME;
        result = new StringBuilder();
        Cursor cursor = ourDatabase.rawQuery(grpQuery, null);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            result.append(cursor.getInt(0))
                    .append("_")
                    .append(cursor.getString(1))
                    .append("&");
        }

        close();

        return result.toString();
    }

    public void delete() {
        open();

        String tables[] = {TABLE_NAME};

        Log.d("deleting db", "111");
        Log.d("table length", "" + tables.length);

        for (int i = 0; i < tables.length; i++) {
            String tquery = "SELECT * FROM " + tables[i];
            Cursor cursor = ourDatabase.rawQuery(tquery, null);
            Log.i(DEBUG_TAG, "delete query :" + tquery);
            Log.i(DEBUG_TAG, "cursor count :" + cursor.getCount());
            if (cursor.getCount() != 0) {

                String deleteQuery = "delete from " + tables[i];
                ourDatabase.execSQL(deleteQuery);
            }
        }
        close();
    }

    public void checkUpdateDatabase(QRUploadModel qrUploadModel) {
        open();
        for (QRUploadModel.StatusList data : qrUploadModel.getStatuslist()) {
            ourDatabase.execSQL("delete from " + TABLE_NAME + " where id='" + data.getLocalid() + "'");
        }
        close();
    }

    public class DBHelper extends SQLiteOpenHelper {

        public DBHelper(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {

        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            onCreate(db);
        }

        /**
         * Creates a empty database on the system and copies the db from assets
         **/
        public void createDataBase() throws IOException {

            Log.i(DEBUG_TAG, "Creating database.");
            boolean dbExist = checkDataBase();
            if (dbExist) {

            } else {
                this.getReadableDatabase();
                try {

                    copyDataBase();
                } catch (IOException e) {
                    Log.i(DEBUG_TAG, "caught io exception");
                    e.printStackTrace();
                }
            }
        }

        private boolean checkDataBase() {

            boolean checkdb = false;
            try {
                String myPath = ourContext.getFilesDir().getAbsolutePath()
                        .replace("files", "databases")
                        + File.separator + DATABASE_NAME;
                File dbfile = new File(myPath);
                checkdb = dbfile.exists();
            } catch (SQLiteException e) {

            }

            return checkdb;

        }

        /**
         * Copies your database from your assets-folder to system folder
         **/
        private void copyDataBase() throws IOException {

            InputStream myInput = ourContext.getAssets().open(
                    DATABASE_NAME + ".sqlite");
            String outFileName = DB_PATH + "";

            OutputStream myOutput = new FileOutputStream(outFileName);

            byte[] buffer = new byte[1024];
            int length;
            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            // Close the streams
            myOutput.flush();
            myOutput.close();
            myInput.close();

        }

    }
}