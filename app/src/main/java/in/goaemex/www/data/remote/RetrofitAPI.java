package in.goaemex.www.data.remote;

import in.goaemex.www.dashboard.VersionValidity;
import in.goaemex.www.dashboard.edit_profile.EditProfileModel;
import in.goaemex.www.dashboard.employer_attendance.EmployerAttendanceModel;
import in.goaemex.www.dashboard.interview_status.InterviewStatusModel;
import in.goaemex.www.dashboard.profile.ProfileModel;
import in.goaemex.www.dashboard.qr_scan.QRUploadModel;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.login.LoginModel;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

/**
 * Created by lenovo on 22-03-2018.
 */

public interface RetrofitAPI {

    @POST("/api/ScanData/LoggedInUser")
    Call<LoginModel> loginUser(@Body RequestBody requestBody);

    @POST("/api/ScanData/ScannedUser")
    Call<QRUploadModel> uploadQRData(@Body RequestBody requestBody);

    @GET("/api/ScanData/ScanLog")
    Call<LogModel> getScanLog(@Query("userid") String userid);


    @POST("/api/ScanData/UpdateUserDetails")
    Call<ProfileModel> getProfileData(@Body RequestBody requestBody);

    @POST("/api/ScanData/UpdateUserDetails")
    Call<EditProfileModel> updateProfile(@Body RequestBody requestBody);

    @GET("/api/ScanData/CompanyData")
    Call<EmployerAttendanceModel> getCompanyData(@Query("userid") String userid);

    @GET("/api/ScanData/UserHistory")
    Call<InterviewStatusModel> getInterviewStatus(@Query("userid") String userid);

    @GET("/api/ScanData/CheckVersion")
    Call<VersionValidity> checkVersionValidity(@Query("versionId")int versionCode);
}