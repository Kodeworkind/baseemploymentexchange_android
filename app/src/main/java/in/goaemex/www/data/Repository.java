package in.goaemex.www.data;


import javax.inject.Inject;
import javax.inject.Singleton;

import in.goaemex.www.dashboard.VersionValidity;
import in.goaemex.www.dashboard.edit_profile.EditProfileModel;
import in.goaemex.www.dashboard.employer_attendance.EmployerAttendanceModel;
import in.goaemex.www.dashboard.interview_status.InterviewStatusModel;
import in.goaemex.www.dashboard.profile.ProfileModel;
import in.goaemex.www.dashboard.qr_scan.QRUploadModel;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.data.scopes.Local;
import in.goaemex.www.data.scopes.Remote;
import in.goaemex.www.login.LoginModel;
import io.reactivex.Single;


@Singleton
public class Repository implements DataSource {
    private final DataSource mRemoteDataSource;
    private final DataSource mLocalDataSource;

    @Inject
    public Repository(@Remote DataSource mRemoteDataSource, @Local DataSource mLocalDataSource) {
        this.mRemoteDataSource = mRemoteDataSource;
        this.mLocalDataSource = mLocalDataSource;
    }

    @Override
    public Single<LoginModel> performLogin(String requestBody) {
        return mRemoteDataSource.performLogin(requestBody);
    }

    @Override
    public Single<LogModel> getScanLog(String userID) {
        return mRemoteDataSource.getScanLog(userID);
    }

    @Override
    public Single<EmployerAttendanceModel> getEmployerLog(String userID) {
        return mRemoteDataSource.getEmployerLog(userID);
    }

    @Override
    public Single<QRUploadModel> sendScanData(String requestBody) {
        return mRemoteDataSource.sendScanData(requestBody);
    }

    @Override
    public boolean saveFailedEntry(String data) {
        return mLocalDataSource.saveFailedEntry(data);
    }

    @Override
    public String getFailedData() {
        return mLocalDataSource.getFailedData();
    }

    @Override
    public void checkUpdateDatabase(QRUploadModel qrUploadModel) {
        mLocalDataSource.checkUpdateDatabase(qrUploadModel);
    }

    @Override
    public Single<ProfileModel> getProfileData(String userEmail) {
        return mRemoteDataSource.getProfileData(userEmail);
    }

    @Override
    public Single<EditProfileModel> updateProfile(String formattedObject) {
        return mRemoteDataSource.updateProfile(formattedObject);
    }

    @Override
    public Single<InterviewStatusModel> getInterviewStatusLog(String userID) {
        return mRemoteDataSource.getInterviewStatusLog(userID);
    }

    @Override
    public Single<VersionValidity> checkVersionValidity(int versionCode) {
        return mRemoteDataSource.checkVersionValidity(versionCode);
    }
}