package in.goaemex.www.data.module;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import in.goaemex.www.data.local.DatabaseHelper;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@Module
public class DatabaseModule {

    @Singleton
    @Provides
    DatabaseHelper provideDatabaseHelper(Application application) {
        return new DatabaseHelper(application);
    }
}
