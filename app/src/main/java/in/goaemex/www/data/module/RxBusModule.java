package in.goaemex.www.data.module;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import in.goaemex.www.data.RxBus;

/**
 * Created by Vaibhav Barad on 02-04-2018.
 */

@Module
public class RxBusModule {

    @Singleton
    @Provides
    RxBus provideRxBus() {
        return new RxBus();
    }
}
