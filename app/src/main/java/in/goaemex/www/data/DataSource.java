package in.goaemex.www.data;

import in.goaemex.www.dashboard.VersionValidity;
import in.goaemex.www.dashboard.edit_profile.EditProfileModel;
import in.goaemex.www.dashboard.employer_attendance.EmployerAttendanceModel;
import in.goaemex.www.dashboard.interview_status.InterviewStatusModel;
import in.goaemex.www.dashboard.profile.ProfileModel;
import in.goaemex.www.dashboard.qr_scan.QRUploadModel;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.login.LoginModel;
import io.reactivex.Single;

/**
 * Main entry point for accessing tasks data.
 */

public interface DataSource {
    Single<LoginModel> performLogin(String requestBody);

    Single<LogModel> getScanLog(String userID);

    Single<EmployerAttendanceModel> getEmployerLog(String userID);

    Single<QRUploadModel> sendScanData(String requestBody);

    boolean saveFailedEntry(String data);

    String getFailedData();

    void checkUpdateDatabase(QRUploadModel qrUploadModel);

    Single<ProfileModel> getProfileData(String formattedObject);

    Single<EditProfileModel> updateProfile(String formattedObject);

    Single<InterviewStatusModel> getInterviewStatusLog(String userID);

    Single<VersionValidity> checkVersionValidity(int versionCode);
}