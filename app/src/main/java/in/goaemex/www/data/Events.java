package in.goaemex.www.data;

import android.view.View;

import in.goaemex.www.dashboard.profile.ProfileModel;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public class Events {

    public static class PermissionEvent {
    }

    public static class ScanEvent {
        String scanData;
        int status;
        String optionalMessage;

        public ScanEvent(String scanData, int status, String optionalMessage) {
            this.scanData = scanData;
            this.status = status;
            this.optionalMessage = optionalMessage;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getOptionalMessage() {
            return optionalMessage;
        }

        public void setOptionalMessage(String optionalMessage) {
            this.optionalMessage = optionalMessage;
        }

        public String getScanData() {
            return scanData;
        }

        public void setScanData(String scanData) {
            this.scanData = scanData;
        }
    }

    public static class EditProfileEvent {
        View[] views;
        private ProfileModel mProfileData;

        public EditProfileEvent(ProfileModel profileModel, View... views) {
            mProfileData = profileModel;
            this.views = views;
        }

        public ProfileModel getmProfileData() {
            return mProfileData;
        }

        public void setmProfileData(ProfileModel mProfileData) {
            this.mProfileData = mProfileData;
        }

        public View[] getViews() {
            return views;
        }

        public void setViews(View[] views) {
            this.views = views;
        }
    }

    public static class LoginEvent {
        String password;
        String username;

        int errorField;
        String errorMessage;

        public LoginEvent(int errorField, String errorMessage) {
            this.errorField = errorField;
            this.errorMessage = errorMessage;
        }

        public LoginEvent(String password, String username) {
            this.password = password;
            this.username = username;
        }

        public int getErrorField() {
            return errorField;
        }

        public void setErrorField(int errorField) {
            this.errorField = errorField;
        }

        public String getErrorMessage() {
            return errorMessage;
        }

        public void setErrorMessage(String errorMessage) {
            this.errorMessage = errorMessage;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getUsername() {
            return username;
        }

        public void setUsername(String username) {
            this.username = username;
        }
    }

    public static class LoginStatus {
        boolean isSuccess;

        public LoginStatus(boolean isSuccess) {
            this.isSuccess = isSuccess;
        }

        public boolean isSuccess() {
            return isSuccess;
        }

        public void setSuccess(boolean success) {
            isSuccess = success;
        }
    }

    public static class PerformLogin {
    }
}
