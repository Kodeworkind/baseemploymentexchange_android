package in.goaemex.www.data.module;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import in.goaemex.www.BuildConfig;
import in.goaemex.www.data.local.SharedPrefsHelper;

/**
 * Created by Vaibhav Barad on 3/29/2018.
 */
@Module
public class SharedPrefModule {

    @Singleton
    @Provides
    SharedPreferences provideSharedPrefs(Application application) {
        return application.getSharedPreferences(BuildConfig.APP_PREFS, Context.MODE_PRIVATE);
    }

    @Singleton
    @Provides
    SharedPrefsHelper provideSharedPrefsHelper(SharedPreferences preferences) {
        return new SharedPrefsHelper(preferences);
    }

}
