package in.goaemex.www.data.local;

import javax.inject.Inject;
import javax.inject.Singleton;

import in.goaemex.www.dashboard.VersionValidity;
import in.goaemex.www.dashboard.edit_profile.EditProfileModel;
import in.goaemex.www.dashboard.employer_attendance.EmployerAttendanceModel;
import in.goaemex.www.dashboard.interview_status.InterviewStatusModel;
import in.goaemex.www.dashboard.profile.ProfileModel;
import in.goaemex.www.dashboard.qr_scan.QRUploadModel;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.data.DataSource;
import in.goaemex.www.login.LoginModel;
import io.reactivex.Single;

/**
 * Concrete implementation of DataSource as a Realm/Room/Sqlite.
 */
@Singleton
public class LocalRepo implements DataSource {
    DatabaseHelper mDatabaseHelper;

    @Inject
    public LocalRepo(DatabaseHelper mDatabaseHelper) {
        this.mDatabaseHelper = mDatabaseHelper;
    }

    @Override
    public Single<LoginModel> performLogin(String requestBody) {
        return null;
    }

    @Override
    public Single<LogModel> getScanLog(String userID) {
        return null;
    }

    @Override
    public Single<EmployerAttendanceModel> getEmployerLog(String userID) {
        return null;
    }

    @Override
    public Single<QRUploadModel> sendScanData(String requestBody) {
        return null;
    }

    @Override
    public boolean saveFailedEntry(String data) {
        return mDatabaseHelper.saveFailedEntry(data);
    }

    @Override
    public String getFailedData() {
        return mDatabaseHelper.getAFailedEntry();
    }

    @Override
    public void checkUpdateDatabase(QRUploadModel qrUploadModel) {
        mDatabaseHelper.checkUpdateDatabase(qrUploadModel);
    }

    @Override
    public Single<ProfileModel> getProfileData(String userEmail) {
        return null;
    }

    @Override
    public Single<EditProfileModel> updateProfile(String formattedObject) {
        return null;
    }

    @Override
    public Single<InterviewStatusModel> getInterviewStatusLog(String userID) {
        return null;
    }

    @Override
    public Single<VersionValidity> checkVersionValidity(int versionCode) {
        return null;
    }
}