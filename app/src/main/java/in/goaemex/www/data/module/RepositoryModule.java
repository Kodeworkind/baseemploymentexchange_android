package in.goaemex.www.data.module;

import javax.inject.Singleton;

import dagger.Binds;
import dagger.Module;
import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;
import in.goaemex.www.data.local.LocalRepo;
import in.goaemex.www.data.remote.RemoteRepo;
import in.goaemex.www.data.scopes.Local;
import in.goaemex.www.data.scopes.Remote;

/**
 * This is used by Dagger to inject the required arguments into the {@link Repository}.
 */
@Module
abstract public class RepositoryModule {

    @Singleton
    @Binds
    @Local
    abstract DataSource provideLocalDataSource(LocalRepo dataSource);

    @Singleton
    @Binds
    @Remote
    abstract DataSource provideRemoteDataSource(RemoteRepo dataSource);

}