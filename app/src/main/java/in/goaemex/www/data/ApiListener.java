package in.goaemex.www.data;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public interface ApiListener<T> {
    void onSuccess(T t);

    void onFailure(String message);
}
