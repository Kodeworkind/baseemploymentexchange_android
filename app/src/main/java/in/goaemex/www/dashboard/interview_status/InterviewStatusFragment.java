package in.goaemex.www.dashboard.interview_status;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import in.goaemex.www.R;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.dashboard.scan_log.ScanHistoryAdapter;
import in.goaemex.www.data.local.SharedPrefsHelper;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.utility.CustomProgressDialog;

import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_ID;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
@ActivityScoped
public class InterviewStatusFragment extends DaggerFragment implements InterviewStatusContract.View {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.errorMessage)
    TextView errorMessage;
    @BindView(R.id.viewFlipper)
    ViewFlipper viewFlipper;


    @Inject
    InterviewStatusContract.Presenter mPresenter;
    @Inject
    SharedPrefsHelper mPrefsHelper;

    @Inject
    public InterviewStatusFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.scan_log_list, container, false);
        ButterKnife.bind(this, rootView);
        mPresenter.takeView(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new InterviewStatusAdapter(getActivity(), new ArrayList<>()));
        viewFlipper.setDisplayedChild(0);
        mPresenter.getInterviewLog(String.valueOf(mPrefsHelper.get(PREF_USER_ID, 0)));
        errorMessage.setText("Looks like you haven't gone for interviews");
        return rootView;
    }

    @Override
    public void onInterviewLogs(InterviewStatusModel statusModel) {
        if (statusModel != null && statusModel.getUserHistory().size() > 0) {
            viewFlipper.setDisplayedChild(1);
            ((InterviewStatusAdapter) recyclerView.getAdapter()).updateData(statusModel.getUserHistory());
        } else {
            viewFlipper.setDisplayedChild(2);
        }
    }

    @Override
    public void scanLogFailure(String message) {
        viewFlipper.setDisplayedChild(2);
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressLoader(boolean toShow) {
        if(toShow){
            CustomProgressDialog.showProgressDialog(getActivity(),null);
        }else{
            CustomProgressDialog.hideProgressDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.dropView();
    }
}
