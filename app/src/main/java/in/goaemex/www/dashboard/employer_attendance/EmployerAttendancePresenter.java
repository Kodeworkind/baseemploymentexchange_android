package in.goaemex.www.dashboard.employer_attendance;

import javax.inject.Inject;

import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static in.goaemex.www.utility.ObjectUtil.isNull;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public class EmployerAttendancePresenter implements EmployerAttendanceContract.Presenter {


    private DataSource mDataSource;
    private Disposable mDisposable;
    private EmployerAttendanceContract.View mView;
    private CompositeDisposable mCompositeDisposable;


    @Inject
    public EmployerAttendancePresenter(Repository mDataSource, CompositeDisposable mCompositeDisposable) {
        this.mDataSource = mDataSource;
        this.mCompositeDisposable = mCompositeDisposable;
    }

    @Override
    public void getEmployerAttendanceLog(String userID, int pageNo) {
        mView.showProgressLoader(true);
        DisposableSingleObserver<EmployerAttendanceModel> mSingleObserver = new DisposableSingleObserver<EmployerAttendanceModel>() {
            @Override
            public void onSuccess(EmployerAttendanceModel logModel) {
                mView.showProgressLoader(false);
                if (!isNull(logModel))
                    mView.onEmployerAttendanceData(logModel);
                else
                    mView.employerAttendanceLogFailure("Looks like we are having some problems");
            }

            @Override
            public void onError(Throwable e) {
                mView.showProgressLoader(false);
                mView.employerAttendanceLogFailure("Looks like we are having some problems");
            }
        };
        if (!mCompositeDisposable.isDisposed()) {
            Single<EmployerAttendanceModel> employerLog = mDataSource.getEmployerLog(userID);
            mDisposable = employerLog.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(mSingleObserver);
            mCompositeDisposable.add(mSingleObserver);
            mCompositeDisposable.add(mDisposable);
        }

    }

    @Override
    public void takeView(EmployerAttendanceContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.remove(mDisposable);
            mCompositeDisposable.clear();
        }
    }
}
