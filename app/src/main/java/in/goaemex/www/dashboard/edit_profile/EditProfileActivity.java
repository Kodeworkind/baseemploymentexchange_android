package in.goaemex.www.dashboard.edit_profile;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import in.goaemex.www.R;
import in.goaemex.www.dashboard.profile.ProfileModel;
import in.goaemex.www.qualification_search.QualificationSearchActivity;
import in.goaemex.www.utility.CustomProgressDialog;

import static in.goaemex.www.utility.ObjectUtil.isNull;

/**
 * Created by Vaibhav Barad on 5/26/2018.
 */
public class EditProfileActivity extends DaggerAppCompatActivity implements EditProfileContract.View {

    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @BindView(R.id.save)
    TextView save;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.tv_name_value)
    EditText tvNameValue;
    @BindView(R.id.tv_mName_value)
    EditText tvMNameValue;
    @BindView(R.id.tv_lName_value)
    EditText tvLNameValue;
    @BindView(R.id.tv_address_value)
    EditText tvAddressValue;
    @BindView(R.id.tv_mobile_value)
    EditText tvMobileValue;
    @BindView(R.id.tv_employment_no_value)
    EditText tvEmploymentNoValue;
    @BindView(R.id.tv_qualification_value)
    TextView tvQualificationValue;
    @BindView(R.id.tv_experience)
    TextView tvExperience;
    @BindView(R.id.tv_employment_status_value)
    Spinner tvEmploymentStatusValue;
    @BindView(R.id.tv_experience_value)
    EditText tvExperienceValue;

    ProfileModel mProfileData, editData;
    String employmentStatusValue, qualificationValue = "";

    @Inject
    EditProfileContract.Presenter mPresenter;
    @Inject
    Gson gson;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setInitialData();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    private void setInitialData() {
        mProfileData = getIntent().getParcelableExtra("user_data");
        if (!isNull(mProfileData)) {
            editData = mProfileData;
            tvNameValue.setText(mProfileData.getUserDetails().getName());
            tvLNameValue.setText(mProfileData.getUserDetails().getSurname());
            tvMNameValue.setText(mProfileData.getUserDetails().getMiddleName());
            tvAddressValue.setText(mProfileData.getUserDetails().getAddress());
            tvMobileValue.setText(mProfileData.getUserDetails().getMobileNo());

            tvEmploymentNoValue.setText(mProfileData.getUserDetails().getEmploymentCardNo());

            qualificationValue = mProfileData.getUserDetails().getQualification();
            tvQualificationValue.setText(qualificationValue);

            int selectedPos = 0;
            employmentStatusValue = mProfileData.getUserDetails().getEmploymentStatus();
            if (employmentStatusValue.equalsIgnoreCase("employed")) {
                tvExperienceValue.setEnabled(false);
                tvExperienceValue.setText(mProfileData.getUserDetails().getEmploymentStatus());
            } else {
                selectedPos = 1;
            }

            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.employment_status, android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            tvEmploymentStatusValue.setAdapter(adapter);
            tvEmploymentStatusValue.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    employmentStatusValue = (String) parent.getItemAtPosition(position);
                    if (employmentStatusValue.equalsIgnoreCase("employed")) {
                        tvExperienceValue.setEnabled(true);
                        tvExperienceValue.setText("0");
                    } else {
                        tvExperienceValue.setEnabled(true);
                        tvExperienceValue.setText("");
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });
            tvEmploymentStatusValue.setSelection(selectedPos);

        }
    }

    @OnClick(R.id.save)
    public void onViewClicked() {
        editData.getUserDetails().setName(tvNameValue.getText().toString());
        editData.getUserDetails().setSurname(tvLNameValue.getText().toString());
        editData.getUserDetails().setMiddleName(tvMNameValue.getText().toString());
        editData.getUserDetails().setAddress(tvAddressValue.getText().toString());
        editData.getUserDetails().setMobileNo(tvMobileValue.getText().toString());
        editData.getUserDetails().setEmploymentCardNo(tvEmploymentNoValue.getText().toString());
        editData.getUserDetails().setEmploymentStatus(employmentStatusValue);
        editData.getUserDetails().setYearsOfExperience(tvExperienceValue.getText().toString().isEmpty() ? "0" : tvExperienceValue.getText().toString());
        editData.getUserDetails().setQualification(qualificationValue);
        mPresenter.validateData(editData);
    }

    @OnClick(R.id.tv_qualification_value)
    public void editQualificationClicked() {
        startActivityForResult(new Intent(this, QualificationSearchActivity.class), 80);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (RESULT_OK == resultCode && 80 == requestCode) {
            qualificationValue = data.getStringExtra("qualification");
            tvQualificationValue.setText(qualificationValue);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            finishAfterTransition();
        } else {
            finish();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPresenter.dropView();
    }

    @Override
    public void showError(int fieldPosition, String message) {
        Toast.makeText(EditProfileActivity.this, message, Toast.LENGTH_SHORT).show();
        switch (fieldPosition) {
            case 1:
                tvNameValue.setError(message);
                break;
            case 3:
                tvLNameValue.setError(message);
                break;
            case 4:
                tvAddressValue.setError(message);
                break;
            case 5:
                tvMobileValue.setError(message);
                break;
            case 7:
                tvQualificationValue.setError(message);
                break;
            default:
                Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onProfileUpdateSuccess(ProfileModel userModel) {
        Intent intent = new Intent();
        intent.putExtra("user_data", userModel);
        setResult(RESULT_OK, intent);
        finish();
    }

    @Override
    public void formatProfileData(ProfileModel profileModel) {
        JSONObject parentObject = null;
        try {
            parentObject = new JSONObject();
            parentObject.put("UserDetails", new JSONObject(gson.toJson(profileModel.getUserDetails()))).put("isEdit", true).put("timestamp", String.valueOf(System.currentTimeMillis()));
            Log.e("Edit profile", parentObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
            mPresenter.updateProfileData("");
        }

        mPresenter.updateProfileData(parentObject.toString());
    }

    @Override
    public void showProgressLoader(boolean toShow) {
        if (toShow) {
            CustomProgressDialog.showProgressDialog(this, null);
        } else {
            CustomProgressDialog.hideProgressDialog();
        }
    }
}
