package in.goaemex.www.dashboard.edit_profile;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import in.goaemex.www.di.ActivityScoped;

/**
 * Created by Vaibhav Barad on 5/27/2018.
 */
@Module
public abstract class EditProfileModule {

    @ActivityScoped
    @Binds
    abstract  EditProfileContract.View view(EditProfileActivity editProfileActivity);

    @ActivityScoped
    @Binds
    abstract  EditProfileContract.Presenter presenter(EditProfilePresenter editProfilePresenter);
}
