package in.goaemex.www.dashboard.employer_attendance;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public class EmployerAttendanceModel implements Parcelable {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("CompanyDetails")
    @Expose
    public ArrayList<CompanyDetail> companyDetails = null;
    @SerializedName("message")
    @Expose
    public String message;

    protected EmployerAttendanceModel(Parcel in) {
        status = in.readString();
        companyDetails = in.createTypedArrayList(CompanyDetail.CREATOR);
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeTypedList(companyDetails);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<EmployerAttendanceModel> CREATOR = new Creator<EmployerAttendanceModel>() {
        @Override
        public EmployerAttendanceModel createFromParcel(Parcel in) {
            return new EmployerAttendanceModel(in);
        }

        @Override
        public EmployerAttendanceModel[] newArray(int size) {
            return new EmployerAttendanceModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<CompanyDetail> getCompanyDetails() {
        return companyDetails;
    }

    public void setCompanyDetails(ArrayList<CompanyDetail> companyDetails) {
        this.companyDetails = companyDetails;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class CompanyDetail implements Parcelable{

        @SerializedName("companyID")
        @Expose
        public int companyID;
        @SerializedName("Name")
        @Expose
        public String name;
        @SerializedName("recruiterDetails")
        @Expose
        public ArrayList<RecruiterDetail> recruiterDetails = null;

        protected CompanyDetail(Parcel in) {
            companyID = in.readInt();
            name = in.readString();
            recruiterDetails = in.createTypedArrayList(RecruiterDetail.CREATOR);
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(companyID);
            dest.writeString(name);
            dest.writeTypedList(recruiterDetails);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<CompanyDetail> CREATOR = new Creator<CompanyDetail>() {
            @Override
            public CompanyDetail createFromParcel(Parcel in) {
                return new CompanyDetail(in);
            }

            @Override
            public CompanyDetail[] newArray(int size) {
                return new CompanyDetail[size];
            }
        };

        public int getCompanyID() {
            return companyID;
        }

        public void setCompanyID(int companyID) {
            this.companyID = companyID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public ArrayList<RecruiterDetail> getRecruiterDetails() {
            return recruiterDetails;
        }

        public void setRecruiterDetails(ArrayList<RecruiterDetail> recruiterDetails) {
            this.recruiterDetails = recruiterDetails;
        }

        public static class RecruiterDetail implements Parcelable{

            @SerializedName("UserID")
            @Expose
            public int userID;
            @SerializedName("Name")
            @Expose
            public String name;
            @SerializedName("IsAttended")
            @Expose
            public boolean isAttended;
            @SerializedName("PhoneNo")
            @Expose
            public String phoneNo;

            protected RecruiterDetail(Parcel in) {
                userID = in.readInt();
                name = in.readString();
                isAttended = in.readByte() != 0;
                phoneNo = in.readString();
            }

            @Override
            public void writeToParcel(Parcel dest, int flags) {
                dest.writeInt(userID);
                dest.writeString(name);
                dest.writeByte((byte) (isAttended ? 1 : 0));
                dest.writeString(phoneNo);
            }

            @Override
            public int describeContents() {
                return 0;
            }

            public static final Creator<RecruiterDetail> CREATOR = new Creator<RecruiterDetail>() {
                @Override
                public RecruiterDetail createFromParcel(Parcel in) {
                    return new RecruiterDetail(in);
                }

                @Override
                public RecruiterDetail[] newArray(int size) {
                    return new RecruiterDetail[size];
                }
            };

            public String getPhoneNo() {
                return phoneNo;
            }

            public void setPhoneNo(String phoneNo) {
                this.phoneNo = phoneNo;
            }

            public int getUserID() {
                return userID;
            }

            public void setUserID(int userID) {
                this.userID = userID;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public boolean isAttended() {
                return isAttended;
            }

            public void setAttended(boolean attended) {
                isAttended = attended;
            }
        }
    }

}
