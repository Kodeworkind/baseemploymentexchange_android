package in.goaemex.www.dashboard.scan_log;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.di.FragmentScoped;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@Module
public abstract class ScanLogModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract ScanLogFragment logFragment();

    @ActivityScoped
    @Binds
    abstract ScanLogContract.Presenter presenter(ScanLogPresenter presenter);
}
