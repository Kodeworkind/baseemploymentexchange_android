package in.goaemex.www.dashboard.qr_scan;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vaibhav Barad on 1/25/2018.
 */

public class QRUploadModel {

    @SerializedName("StatusList")
    @Expose
    private ArrayList<StatusList> Statuslist;

    public ArrayList<StatusList> getStatuslist() {
        return Statuslist;
    }

    public void setStatuslist(ArrayList<StatusList> statuslist) {
        Statuslist = statuslist;
    }

    public static class StatusList {
        @SerializedName("localid")
        @Expose
        private int localid;
        @SerializedName("isUploaded")
        @Expose
        private boolean isUploaded;

        public int getLocalid() {
            return localid;
        }

        public void setLocalid(int localid) {
            this.localid = localid;
        }

        public boolean isUploaded() {
            return isUploaded;
        }

        public void setUploaded(boolean uploaded) {
            isUploaded = uploaded;
        }
    }
}
