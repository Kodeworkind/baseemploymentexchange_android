package in.goaemex.www.dashboard.employer_attendance;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.goaemex.www.R;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
class CompanyRecruiterAdapter extends RecyclerView.Adapter<CompanyRecruiterAdapter.ViewHolder> {
    private final Context mContext;
    private final ArrayList<EmployerAttendanceModel.CompanyDetail.RecruiterDetail> recruiterDetails;


    public CompanyRecruiterAdapter(Context mContext, ArrayList<EmployerAttendanceModel.CompanyDetail.RecruiterDetail> recruiterDetails) {
        this.mContext = mContext;
        this.recruiterDetails = recruiterDetails;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.company_recruiter_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(recruiterDetails.get(position).getName());
        holder.contact.setText(recruiterDetails.get(position).getPhoneNo());
        if (recruiterDetails.get(position).isAttended())
            holder.check.setVisibility(View.VISIBLE);
        else
            holder.check.setVisibility(View.GONE);

        holder.call.setOnClickListener(view -> {
            Intent intent = new Intent(Intent.ACTION_DIAL);
            intent.setData(Uri.parse("tel:" + recruiterDetails.get(position).getPhoneNo()));
            mContext.startActivity(intent);
        });

    }

    @Override
    public int getItemCount() {
        return recruiterDetails.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.contact)
        TextView contact;
        @BindView(R.id.call)
        ImageView call;
        @BindView(R.id.check)
        ImageView check;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
