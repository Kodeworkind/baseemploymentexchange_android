package in.goaemex.www.dashboard.scan_log;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.goaemex.www.R;

/**
 * Created by Vaibhav Barad on 1/29/2018.
 */

public class ScanHistoryViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.name)
    public TextView name;
    @BindView(R.id.email)
    public TextView email;
    @BindView(R.id.phone)
    public TextView phone;
    @BindView(R.id.message)
    public TextView message;
    @BindView(R.id.messageLabel)
    public TextView messageLabel;

    public ScanHistoryViewHolder(View view) {
        super(view);
        ButterKnife.bind(this, view);
    }
}
