package in.goaemex.www.dashboard.interview_status;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
public class InterviewStatusModel implements Parcelable{

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("UserHistory")
    @Expose
    public ArrayList<UserHistory> userHistory = null;
    @SerializedName("message")
    @Expose
    public String message;

    protected InterviewStatusModel(Parcel in) {
        status = in.readString();
        userHistory = in.createTypedArrayList(UserHistory.CREATOR);
        message = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeTypedList(userHistory);
        dest.writeString(message);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<InterviewStatusModel> CREATOR = new Creator<InterviewStatusModel>() {
        @Override
        public InterviewStatusModel createFromParcel(Parcel in) {
            return new InterviewStatusModel(in);
        }

        @Override
        public InterviewStatusModel[] newArray(int size) {
            return new InterviewStatusModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public ArrayList<UserHistory> getUserHistory() {
        return userHistory;
    }

    public void setUserHistory(ArrayList<UserHistory> userHistory) {
        this.userHistory = userHistory;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public static class UserHistory implements Parcelable{

        @SerializedName("CompanyID")
        @Expose
        public int companyID;
        @SerializedName("CompanyName")
        @Expose
        public String companyName;
        @SerializedName("StatusID")
        @Expose
        public int statusID;

        protected UserHistory(Parcel in) {
            companyID = in.readInt();
            companyName = in.readString();
            statusID = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(companyID);
            dest.writeString(companyName);
            dest.writeInt(statusID);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<UserHistory> CREATOR = new Creator<UserHistory>() {
            @Override
            public UserHistory createFromParcel(Parcel in) {
                return new UserHistory(in);
            }

            @Override
            public UserHistory[] newArray(int size) {
                return new UserHistory[size];
            }
        };

        public int getCompanyID() {
            return companyID;
        }

        public void setCompanyID(int companyID) {
            this.companyID = companyID;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public int getStatusID() {
            return statusID;
        }

        public void setStatusID(int statusID) {
            this.statusID = statusID;
        }
    }
}
