package in.goaemex.www.dashboard.scan_log;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.style.RelativeSizeSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;

import in.goaemex.www.R;
import in.goaemex.www.utility.UtilityClass;

/**
 * Created by Vaibhav Barad on 1/29/2018.
 */

public class ScanHistoryAdapter extends RecyclerView.Adapter {
    Context mContext;
    ArrayList<LogModel.LogData> scanList;

    public ScanHistoryAdapter(Context context, ArrayList<LogModel.LogData> scanList) {
        this.mContext = context;
        this.scanList = scanList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.scan_history, parent, false);
        return new ScanHistoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final LogModel.LogData logData = scanList.get(position);
        if (holder instanceof ScanHistoryViewHolder) {
            ((ScanHistoryViewHolder) holder).phone.setText(logData.getMobile());
            ((ScanHistoryViewHolder) holder).email.setText(logData.getEmail());

            if (logData.getStatus() != 0) {
                String text = logData.getName().trim() + " (" + UtilityClass.getInterviewStatus(logData.getStatus()) + ")";
                SpannableString status = new SpannableString(text.toUpperCase());
                status.setSpan(new RelativeSizeSpan(0.6f), logData.getName().length() + 1, status.length(), 0); // set size
                ((ScanHistoryViewHolder) holder).name.setText(status);
            } else
                ((ScanHistoryViewHolder) holder).name.setText(logData.getName().trim());

            ((ScanHistoryViewHolder) holder).phone.setOnClickListener(view -> {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + logData.getMobile()));
                mContext.startActivity(intent);
            });

            if (logData.getShortText() != null && !logData.getShortText().isEmpty()) {
                ((ScanHistoryViewHolder) holder).message.setVisibility(View.VISIBLE);
                ((ScanHistoryViewHolder) holder).messageLabel.setVisibility(View.VISIBLE);
                ((ScanHistoryViewHolder) holder).message.setText(String.format("%s", logData.getShortText()));
            } else {
                ((ScanHistoryViewHolder) holder).message.setVisibility(View.GONE);
                ((ScanHistoryViewHolder) holder).messageLabel.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return scanList.size();
    }

    public void updateData(ArrayList<LogModel.LogData> scanList) {
        this.scanList = scanList;
        notifyDataSetChanged();
    }
}
