package in.goaemex.www.dashboard.employer_attendance;

import dagger.Binds;
import dagger.Module;
import dagger.Provides;
import dagger.android.ContributesAndroidInjector;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.di.FragmentScoped;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@Module
public abstract class EmployerAttendanceModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract EmployerAttendanceFragment employerAttendanceFragment();

    @Binds
    @ActivityScoped
    abstract EmployerAttendanceContract.Presenter presenter(EmployerAttendancePresenter attendancePresenter);


}
