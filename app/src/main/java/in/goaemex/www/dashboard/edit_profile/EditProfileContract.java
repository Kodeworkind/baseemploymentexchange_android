package in.goaemex.www.dashboard.edit_profile;

import in.goaemex.www.base_class.BasePresenter;
import in.goaemex.www.base_class.BaseView;
import in.goaemex.www.dashboard.profile.ProfileModel;

/**
 * Created by Vaibhav Barad on 5/26/2018.
 */
public interface EditProfileContract {
    interface View extends BaseView<Presenter>{
        void showError(int fieldPosition,String message);

        void onProfileUpdateSuccess(ProfileModel userModel);

        void formatProfileData(ProfileModel profileModel);
    }
    interface Presenter extends BasePresenter<View>{
        void validateData(ProfileModel profileModel);

        void updateProfileData(String formattedObject);
    }
}
