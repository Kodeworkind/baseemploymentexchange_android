package in.goaemex.www.dashboard.interview_status;

import in.goaemex.www.base_class.BasePresenter;
import in.goaemex.www.base_class.BaseView;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.dashboard.scan_log.ScanLogContract;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
public interface InterviewStatusContract {

    interface View extends BaseView<ScanLogContract.Presenter> {

        void onInterviewLogs(InterviewStatusModel statusModel);

        void scanLogFailure(String message);
    }

    interface Presenter extends BasePresenter<View> {

        void getInterviewLog(String userID);
    }
}
