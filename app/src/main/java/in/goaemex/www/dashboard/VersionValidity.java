package in.goaemex.www.dashboard;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Vaibhav Barad on 5/30/2018.
 */
public class VersionValidity {
    @SerializedName("isValid")
    @Expose
    public String isValid;

    public String isValid() {
        return isValid;
    }

    public void setValid(String valid) {
        isValid = valid;
    }
}
