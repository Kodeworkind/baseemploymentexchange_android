package in.goaemex.www.dashboard.employer_attendance;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.goaemex.www.R;
import in.goaemex.www.utility.DividerItemDecoration;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
class EmployerAttendanceLogAdapter extends RecyclerView.Adapter<EmployerAttendanceLogAdapter.ViewHolder> {

    Context mContext;
    ArrayList<EmployerAttendanceModel.CompanyDetail> scanList;

    EmployerAttendanceLogAdapter(Context context, ArrayList<EmployerAttendanceModel.CompanyDetail> scanList) {
        this.mContext = context;
        this.scanList = scanList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.attendance_employer_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.name.setText(scanList.get(position).getName());
        holder.recyclerView.setLayoutManager(new LinearLayoutManager(mContext));
        holder.recyclerView.setAdapter(new CompanyRecruiterAdapter(mContext, scanList.get(position).getRecruiterDetails()));
        holder.recyclerView.addItemDecoration(new DividerItemDecoration(holder.recyclerView.getContext(), null, false, false));
        holder.recyclerView.setHasFixedSize(true);
        holder.check.setVisibility(View.INVISIBLE);
        for (EmployerAttendanceModel.CompanyDetail.RecruiterDetail detail :
                scanList.get(position).getRecruiterDetails()) {
            if (detail.isAttended) {
                holder.check.setVisibility(View.VISIBLE);
                break;
            }
        }
    }


    @Override
    public int getItemCount() {
        return scanList.size();
    }

    public void onDataUpdate(ArrayList<EmployerAttendanceModel.CompanyDetail> attendanceList) {
        scanList.addAll(attendanceList);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.name)
        TextView name;
        @BindView(R.id.recyclerView)
        RecyclerView recyclerView;
        @BindView(R.id.check)
        ImageView check;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
