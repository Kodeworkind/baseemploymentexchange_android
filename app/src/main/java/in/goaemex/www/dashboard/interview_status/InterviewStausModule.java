package in.goaemex.www.dashboard.interview_status;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import in.goaemex.www.dashboard.scan_log.ScanLogContract;
import in.goaemex.www.dashboard.scan_log.ScanLogPresenter;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.di.FragmentScoped;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@Module
public abstract class InterviewStausModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract InterviewStatusFragment statusFragment();

    @ActivityScoped
    @Binds
    abstract InterviewStatusContract.Presenter presenter(InterviewStatusPresenter presenter);
}
