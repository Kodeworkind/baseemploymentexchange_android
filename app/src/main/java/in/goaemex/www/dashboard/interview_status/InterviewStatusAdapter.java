package in.goaemex.www.dashboard.interview_status;

import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.goaemex.www.R;
import in.goaemex.www.utility.UtilityClass;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
class InterviewStatusAdapter extends RecyclerView.Adapter<InterviewStatusAdapter.ViewHolder> {
    private final FragmentActivity activity;
    private ArrayList<InterviewStatusModel.UserHistory> userHistories;

    public InterviewStatusAdapter(FragmentActivity activity, ArrayList<InterviewStatusModel.UserHistory> userHistories) {
        this.activity = activity;
        this.userHistories = userHistories;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.interview_status_item, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.companyName.setText(userHistories.get(position).getCompanyName());
        holder.interviewStatus.setText(UtilityClass.getInterviewStatus(userHistories.get(position).getStatusID()));
    }

    @Override
    public int getItemCount() {
        return userHistories.size();
    }

    public void updateData(ArrayList<InterviewStatusModel.UserHistory> userHistory) {
        this.userHistories = userHistory;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.company_name)
        TextView companyName;
        @BindView(R.id.interview_status)
        TextView interviewStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }
}
