package in.goaemex.www.dashboard.edit_profile;

import javax.inject.Inject;

import in.goaemex.www.dashboard.profile.ProfileModel;
import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vaibhav Barad on 5/26/2018.
 */
public class EditProfilePresenter implements EditProfileContract.Presenter {

    EditProfileContract.View mView;
    CompositeDisposable mCompositeDisposable;
    Disposable mDisposable;
    DataSource mDataSource;
    private ProfileModel validProfileMode;

    @Inject
    public EditProfilePresenter(EditProfileContract.View mView, CompositeDisposable mCompositeDisposable, Repository mDataSource) {
        this.mView = mView;
        this.mCompositeDisposable = mCompositeDisposable;
        this.mDataSource = mDataSource;
    }

    @Override
    public void validateData(ProfileModel profileModel) {
        validProfileMode = profileModel;
        if (profileModel.getUserDetails().getName().equals("")) {
            mView.showError(1, "Please enter your first name");
            return;
        }
        if (profileModel.getUserDetails().getSurname().equals("")) {
            mView.showError(3, "Please enter your last name");
            return;
        }
        if (profileModel.getUserDetails().getAddress().equals("")) {
            mView.showError(4, "Please enter your address");
            return;
        }
        if (profileModel.getUserDetails().getMobileNo().equals("") && profileModel.getUserDetails().getMobileNo().length() < 10) {
            mView.showError(5, "Please enter your mobile number");
            return;
        }
        if (profileModel.getUserDetails().getQualification().equals("")) {
            mView.showError(7, "Please enter your qualification");
            return;
        }
        if (profileModel.getUserDetails().getEmploymentStatus().equals("")) {
            mView.showError(0, "Please enter your employment status");
            return;
        }
        mView.formatProfileData(profileModel);

    }

    @Override
    public void updateProfileData(String formattedObject) {
        mView.showProgressLoader(true);
        DisposableSingleObserver<EditProfileModel> disposableSingleObserver = new DisposableSingleObserver<EditProfileModel>() {
            @Override
            public void onSuccess(EditProfileModel userModel) {
                mView.showProgressLoader(false);
                if (userModel != null && userModel.getStatus().equalsIgnoreCase("success"))
                    mView.onProfileUpdateSuccess(validProfileMode);
                else mView.showError(0, "There was some error");
            }

            @Override
            public void onError(Throwable e) {
                mView.showProgressLoader(false);
                mView.showError(0, e.getMessage());
            }
        };

        if (!mCompositeDisposable.isDisposed()) {
            Single<EditProfileModel> mUserSingle = mDataSource.updateProfile(formattedObject);
            mDisposable = mUserSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(disposableSingleObserver);
            mCompositeDisposable.add(disposableSingleObserver);
            mCompositeDisposable.add(mDisposable);
        }
    }

    @Override
    public void takeView(EditProfileContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.remove(mCompositeDisposable);
            mCompositeDisposable.clear();
        }
    }
}
