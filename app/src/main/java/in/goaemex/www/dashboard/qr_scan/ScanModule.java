package in.goaemex.www.dashboard.qr_scan;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import in.goaemex.www.di.FragmentScoped;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
@Module
public abstract class ScanModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract ScannerFragment scannerFragment();
}
