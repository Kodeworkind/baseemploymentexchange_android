package in.goaemex.www.dashboard.employer_attendance;

import in.goaemex.www.base_class.BasePresenter;
import in.goaemex.www.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public interface EmployerAttendanceContract {

    interface View extends BaseView<Presenter> {

        void onEmployerAttendanceData(EmployerAttendanceModel data);

        void employerAttendanceLogFailure(String message);

    }

    interface Presenter extends BasePresenter<View> {
        void getEmployerAttendanceLog(String userID, int pageNo);
    }
}
