package in.goaemex.www.dashboard.profile;

import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;

import in.goaemex.www.BuildConfig;
import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;

import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static in.goaemex.www.utility.ObjectUtil.isNull;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public class ProfilePresenter implements ProfileContract.Presenter {

    public static final String TAG = ProfilePresenter.class.getSimpleName();
    private ProfileContract.View mView;
    private DataSource mDataSource;

    private CompositeDisposable mCompositeDisposable;
    private Disposable mDisposable;

    @Inject
    public ProfilePresenter(Repository mDataSource, CompositeDisposable compositeDisposable) {
        this.mDataSource = mDataSource;
        this.mCompositeDisposable = compositeDisposable;
    }


    @Override
    public void takeView(ProfileContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.remove(mDisposable);
            mCompositeDisposable.clear();
        }
    }

    @Override
    public void getProfileData(String userEmail) {
        mView.showProgressLoader(true);
        DisposableSingleObserver<ProfileModel> mSingleObserver = new DisposableSingleObserver<ProfileModel>() {
            @Override
            public void onSuccess(ProfileModel profileModel) {
                if (!isNull(profileModel) && profileModel.getStatus().equalsIgnoreCase("success")) {
                    profileModel.getUserDetails()
                            .setQRImage(BuildConfig.BASE_URL.concat("/Content/qrcode/")
                                    .concat(String.valueOf(profileModel.getUserDetails().getUserID()))
                                    .concat(".png"));
                    mView.onUserData(profileModel);
                } else
                    mView.UserDataFailure("Sorry! we are having some issues at the moment");
                mView.showProgressLoader(false);
            }

            @Override
            public void onError(Throwable e) {
                mView.showProgressLoader(false);
                Log.e(TAG,e.getMessage()+":->"+e.getLocalizedMessage());
                mView.UserDataFailure("Sorry! we are having some issues at the moment");
            }
        };
        if (!mCompositeDisposable.isDisposed()) {
            Single<ProfileModel> mLogSingle = mDataSource.getProfileData(getFormatterRequest(userEmail));
            mDisposable = mLogSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(mSingleObserver);
            mCompositeDisposable.add(mSingleObserver);
            mCompositeDisposable.add(mDisposable);
        }
    }

    private String getFormatterRequest(String userEmail) {
        JSONObject parentObject = new JSONObject();
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put("EmailId", userEmail);
            parentObject.put("UserDetails", jsonObject).put("isEdit", false).put("timestamp", String.valueOf(System.currentTimeMillis()));

        } catch (JSONException e) {
            e.printStackTrace();
            return "";
        }
        return parentObject.toString();
    }
}