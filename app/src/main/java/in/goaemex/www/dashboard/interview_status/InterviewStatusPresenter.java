package in.goaemex.www.dashboard.interview_status;

import javax.inject.Inject;

import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static in.goaemex.www.utility.ObjectUtil.isNull;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
public class InterviewStatusPresenter implements InterviewStatusContract.Presenter {
    private InterviewStatusContract.View mView;
    private DataSource mDataSource;
    private Disposable disposable;
    private CompositeDisposable mCompositeDisposable;


    @Inject
    public InterviewStatusPresenter(Repository mDataSource, CompositeDisposable mCompositeDisposable) {
        this.mDataSource = mDataSource;
        this.mCompositeDisposable = mCompositeDisposable;
    }

    @Override
    public void getInterviewLog(String userID) {
        mView.showProgressLoader(true);
        DisposableSingleObserver<InterviewStatusModel> mSingleObserver = new DisposableSingleObserver<InterviewStatusModel>() {
            @Override
            public void onSuccess(InterviewStatusModel statusModel) {
                mView.showProgressLoader(false);
                if (!isNull(statusModel))
                    mView.onInterviewLogs(statusModel);
                else
                    mView.scanLogFailure("Looks like you haven't gone for interviews");
            }

            @Override
            public void onError(Throwable e) {
                mView.showProgressLoader(false);
                mView.scanLogFailure("Looks like you haven't gone for interviews");
            }
        };
        if (!mCompositeDisposable.isDisposed()) {
            Single<InterviewStatusModel> mLogSingle = mDataSource.getInterviewStatusLog(userID);
            disposable = mLogSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(mSingleObserver);
            mCompositeDisposable.add(mSingleObserver);
            mCompositeDisposable.add(disposable);
        }

    }

    @Override
    public void takeView(InterviewStatusContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.remove(disposable);
            mCompositeDisposable.clear();
        }
    }
}
