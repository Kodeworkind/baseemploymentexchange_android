package in.goaemex.www.dashboard.qr_scan;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.Result;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerFragment;
import in.goaemex.www.R;
import in.goaemex.www.data.Events;
import in.goaemex.www.data.RxBus;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.recruiter_option.RecruiterOptionActivity;
import in.goaemex.www.utility.UtilityClass;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import me.dm7.barcodescanner.core.IViewFinder;
import me.dm7.barcodescanner.core.ViewFinderView;
import me.dm7.barcodescanner.zxing.ZXingScannerView;

import static in.goaemex.www.utility.UtilityClass.mRoleType;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */

@ActivityScoped
public class ScannerFragment extends DaggerFragment implements ZXingScannerView.ResultHandler {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    private static final String FLASH_STATE = "FLASH_STATE";
    private static final String AUTO_FOCUS_STATE = "AUTO_FOCUS_STATE";
    private static final String SELECTED_FORMATS = "SELECTED_FORMATS";
    private static final String CAMERA_ID = "CAMERA_ID";

    @BindView(R.id.content_frame)
    FrameLayout contentFrame;

    boolean permissionGranted;
    Bundle bundle;

    @Inject
    RxBus rxBus;
    @Inject
    CompositeDisposable compositeDisposable;

    private ZXingScannerView mScannerView;
    private boolean mFlash;
    private boolean mAutoFocus;
    private ArrayList<Integer> mSelectedIndices;
    private int mCameraId = -1;

    @Inject
    public ScannerFragment() {
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.activity_scanner, container, false);
        bundle = savedInstanceState;
        ButterKnife.bind(this, rootView);


        permission();

        return rootView;
    }

    private void permission() {
        if (UtilityClass.hasPermissions(getActivity(), UtilityClass.PERMISSIONS)) {
            proceedAfterPermission();
        } else {
            Toast.makeText(getActivity(), "Please Grant Permission", Toast.LENGTH_LONG).show();
        }
    }

    private void proceedAfterPermission() {
        if (bundle != null) {
            mFlash = bundle.getBoolean(FLASH_STATE, false);
            mAutoFocus = bundle.getBoolean(AUTO_FOCUS_STATE, true);
            mSelectedIndices = bundle.getIntegerArrayList(SELECTED_FORMATS);
            mCameraId = bundle.getInt(CAMERA_ID, -1);
            permissionGranted = bundle.getBoolean("permissionGranted");
        } else {
            mFlash = false;
            mAutoFocus = true;
            mSelectedIndices = null;
            mCameraId = -1;
        }

        mScannerView = new ZXingScannerView(getActivity()) {
            protected IViewFinder createViewFinderView(Context context) {
                ViewFinderView finderView = new ViewFinderView(context);
                finderView.setLaserColor(Color.TRANSPARENT);
                finderView.setMaskColor(Color.TRANSPARENT);
                return finderView;
            }
        };
        setupFormats();
        contentFrame.addView(mScannerView);
    }

    public void setupFormats() {
        List<BarcodeFormat> formats = new ArrayList<BarcodeFormat>();
        if (mSelectedIndices == null || mSelectedIndices.isEmpty()) {
            mSelectedIndices = new ArrayList<>();
            for (int i = 0; i < ZXingScannerView.ALL_FORMATS.size(); i++) {
                mSelectedIndices.add(i);
            }
        }

        for (int index : mSelectedIndices) {
            formats.add(ZXingScannerView.ALL_FORMATS.get(index));
        }
        if (mScannerView != null) {
            mScannerView.setFormats(formats);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        compositeDisposable.add(rxBus.asFlowable().subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread()).subscribe(object -> {
                    if (object instanceof Events.PermissionEvent)
                        permission();
                }));
        try {
            if (mScannerView != null) {
                mScannerView.setResultHandler(this);
                mScannerView.startCamera(mCameraId);
                //mScannerView.setFlash(mFlash);
                mScannerView.setAutoFocus(mAutoFocus);
            }
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putBoolean(FLASH_STATE, mFlash);
        outState.putBoolean(AUTO_FOCUS_STATE, mAutoFocus);
        outState.putBoolean("permissionGranted", permissionGranted);
        outState.putIntegerArrayList(SELECTED_FORMATS, mSelectedIndices);
        outState.putInt(CAMERA_ID, mCameraId);
    }

    @Override
    public void onPause() {
        super.onPause();
        if (mScannerView != null) {
            mScannerView.stopCamera();
        }
        if (compositeDisposable != null && !compositeDisposable.isDisposed())
            compositeDisposable.dispose();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        compositeDisposable.clear();
    }

    @Override
    public void handleResult(Result rawResult) {
        if (rawResult.getText() != null && !rawResult.getText().isEmpty()) {
            String[] splitData;
            String QRData = rawResult.getText();
            splitData = QRData.split("-");
            if (splitData.length == 2 && splitData[0].equals("goaempex")) {
                if (mRoleType == UtilityClass.UserRole.REC) {
                    Intent intent = new Intent(getActivity(), RecruiterOptionActivity.class);
                    intent.putExtra("qrCode", rawResult.getText());
                    startActivityForResult(intent, 20);
                } else {
                    onPause();
                    if (rxBus.hasObservers()) {
                        rxBus.send(new Events.ScanEvent(rawResult.getText(), 0, ""));
                    }
                    onResume();
                }
            } else {
                Toast.makeText(getActivity(), "The QR code doesn't belong to this event", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (Activity.RESULT_OK == resultCode && 20 == requestCode) {
            if (rxBus.hasObservers()) {
                rxBus.send(new Events.ScanEvent(data.getStringExtra("qrData"), data.getIntExtra("status", 0), data.getStringExtra("optionalMessage")));
            }
        }
    }
}
