package in.goaemex.www.dashboard.scan_log;

import in.goaemex.www.base_class.BasePresenter;
import in.goaemex.www.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public interface ScanLogContract {

    interface View extends BaseView<Presenter>{

        void onScanLogs(LogModel loginModel);

        void scanLogFailure(String message);
    }

    interface Presenter extends BasePresenter<View>{

        void getScanLog(String userID);
    }
}
