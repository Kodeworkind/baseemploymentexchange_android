package in.goaemex.www.dashboard;

import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.util.Pair;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.github.pwittchen.reactivenetwork.library.rx2.ReactiveNetwork;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.concurrent.TimeUnit;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import dagger.android.support.DaggerAppCompatActivity;
import in.goaemex.www.BuildConfig;
import in.goaemex.www.R;
import in.goaemex.www.dashboard.edit_profile.EditProfileActivity;
import in.goaemex.www.dashboard.employer_attendance.EmployerAttendanceFragment;
import in.goaemex.www.dashboard.interview_status.InterviewStatusFragment;
import in.goaemex.www.dashboard.profile.ProfileFragment;
import in.goaemex.www.dashboard.qr_scan.ScannerFragment;
import in.goaemex.www.dashboard.scan_log.ScanLogFragment;
import in.goaemex.www.data.Events;
import in.goaemex.www.data.RxBus;
import in.goaemex.www.data.local.SharedPrefsHelper;
import in.goaemex.www.utility.CustomProgressDialog;
import in.goaemex.www.utility.ForceUpdateDialog;
import in.goaemex.www.utility.UtilityClass;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;

import static android.os.Environment.getExternalStoragePublicDirectory;
import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_ID;
import static in.goaemex.www.utility.UtilityClass.PERMISSION_CALLBACK_CONSTANT;
import static in.goaemex.www.utility.UtilityClass.defineUserRole;
import static in.goaemex.www.utility.UtilityClass.mRoleType;

/**
 * Created by Vaibhav Barad on 5/24/2018.
 */
public class DashboardActivity extends DaggerAppCompatActivity implements DashboardActivityContract.View {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @BindView(R.id.toolBar)
    Toolbar toolBar;
    @BindView(R.id.container)
    FrameLayout container;
    @BindView(R.id.bottomNavigation)
    TabLayout bottomNavigation;
    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;

    Bundle bundle;
    Snackbar snackbar;

    @Inject
    SharedPrefsHelper sharedPrefsHelper;
    @Inject
    RxBus rxBus;

    @Inject
    ScannerFragment mScannerFragment;
    @Inject
    ScanLogFragment mLogFragment;
    @Inject
    EmployerAttendanceFragment attendanceFragment;
    @Inject
    ProfileFragment profileFragment;
    @Inject
    InterviewStatusFragment statusFragment;
    @Inject
    CompositeDisposable scanDisposable;

    @Inject
    DashboardActivityContract.Presenter mPresenter;

    int userID, userRole;
    Disposable internetDisposable;
    boolean isDisposableRegistered;
    boolean showCSV;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        bundle = savedInstanceState;

        setSupportActionBar(toolBar);
        getSupportActionBar().setTitle(null);

        getSharedPreferenceValue();

        initializeTabLayout();
        bottomNavigation.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                getTabFragment(mRoleType, tab.getPosition());

                showCSV = tab.getPosition() == 1;
                invalidateOptionsMenu();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        getTabFragment(mRoleType, 0);
        mPresenter.checkVersionValidity(BuildConfig.VERSION_CODE);

    }

    private void initializeTabLayout() {
        switch (mRoleType) {
            case ORG:
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.scan));
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.log));
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.company_log));
                break;
            case REC:
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.scan));
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.log));
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.profile));
                break;
            case USR:
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.profile));
                bottomNavigation.addTab(bottomNavigation.newTab().setIcon(R.drawable.log));
                break;

        }
    }

    private void getTabFragment(UtilityClass.UserRole mRoleType, int position) {
        if (position == 0 && mRoleType == UtilityClass.UserRole.USR)
            setTabFragment(profileFragment);
        else if (position == 0)
            setTabFragment(mScannerFragment);

        else if (position == 1 && mRoleType == UtilityClass.UserRole.USR)
            setTabFragment(statusFragment);
        else if (position == 1)
            setTabFragment(mLogFragment);

        if (position == 2 && mRoleType == UtilityClass.UserRole.ORG) {
            setTabFragment(attendanceFragment);
        } else if (position == 2)
            setTabFragment(profileFragment);
    }

    private void setTabFragment(Fragment tabFragment) {
        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.replace(R.id.container, tabFragment).commit();
    }

    private void getSharedPreferenceValue() {
        userID = sharedPrefsHelper.get(PREF_USER_ID, 0);
        defineUserRole(sharedPrefsHelper);
    }

    protected void onInternetChange(boolean isConnected) {
        try {
            if (isConnected) {
                mPresenter.syncFailedData();
                if (snackbar.isShown())
                    snackbar.dismiss();

            } else {
                snackbar = Snackbar
                        .make(coordinatorLayout, getResources().getString(R.string.no_internet), Snackbar.LENGTH_LONG);
                snackbar.show();
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mRoleType != UtilityClass.UserRole.USR)
            permission();
        internetDisposable = ReactiveNetwork
                .observeNetworkConnectivity(getApplicationContext())
                .flatMapSingle(connectivity -> {
                    if (connectivity.state() == NetworkInfo.State.CONNECTED) {
                        return ReactiveNetwork.checkInternetConnectivity();
                    }
                    return Single.fromCallable(() -> false);
                })
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .debounce(10, TimeUnit.SECONDS)
                .subscribe(this::onInternetChange);
        if (!isDisposableRegistered) {
            isDisposableRegistered = true;
            scanDisposable.add(rxBus.asFlowable().subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(object -> {
                        if (object instanceof Events.EditProfileEvent) {
                            Intent intent = new Intent(this, EditProfileActivity.class)
                                    .putExtra("user_data", ((Events.EditProfileEvent) object).getmProfileData());
                            View view[] = ((Events.EditProfileEvent) object).getViews();

                            Pair<View, String> p1 = Pair.create(view[0], "fname");
                            Pair<View, String> p2 = Pair.create(view[1], "mname");
                            Pair<View, String> p3 = Pair.create(view[2], "lname");
                            Pair<View, String> p4 = Pair.create(view[3], "address");
                            Pair<View, String> p5 = Pair.create(view[4], "mobile");
                            Pair<View, String> p6 = Pair.create(view[5], "employment_no");
                            Pair<View, String> p7 = Pair.create(view[6], "qualification");
                            Pair<View, String> p8 = Pair.create(view[7], "employment_status");
                            Pair<View, String> p9 = Pair.create(view[8], "experience");

                            ActivityOptionsCompat options = ActivityOptionsCompat.makeSceneTransitionAnimation(DashboardActivity.this,
                                    p1, p2, p3, p4, p5, p6, p7, p8, p9);
                            ActivityCompat.startActivityForResult(DashboardActivity.this, intent, 99, options.toBundle());
                        } else if (object instanceof Events.ScanEvent) {
                            Events.ScanEvent eventdata = (Events.ScanEvent) object;
                            formatScanData(eventdata.getScanData().split("-"),
                                    eventdata.getStatus(), eventdata.getOptionalMessage());
                        }
                    }));
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if (internetDisposable != null && !internetDisposable.isDisposed())
            internetDisposable.dispose();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && requestCode == 99) {
            for (Fragment fragment : getSupportFragmentManager().getFragments()) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void permission() {
        if (!UtilityClass.hasPermissions(this, UtilityClass.PERMISSIONS)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, UtilityClass.PERMISSIONS[0])) {
                Toast.makeText(DashboardActivity.this, " Permissions Required", Toast.LENGTH_SHORT).show();
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle("Need permissions")
                        .setCancelable(false);
                builder.setMessage("We need the  Camera permissions to perform QR scanning");
                builder.setPositiveButton("Grant", (dialog, which) -> {
                    dialog.cancel();
                    ActivityCompat.requestPermissions(DashboardActivity.this, UtilityClass.PERMISSIONS, PERMISSION_CALLBACK_CONSTANT);
                });
                builder.setNegativeButton("Cancel", (dialog, which) -> {
                    Toast.makeText(DashboardActivity.this, "Oops you just denied the Camera permission so cannot open QR scanner", Toast.LENGTH_LONG).show();
                    dialog.cancel();
                    //System.exit(0);
                });
                builder.show();
            } else {
                ActivityCompat.requestPermissions(this, UtilityClass.PERMISSIONS, PERMISSION_CALLBACK_CONSTANT);
            }
        } else {
            proceedAfterPermission();
        }
    }

    private void proceedAfterPermission() {
        switch (mRoleType) {
            default:
                getTabFragment(mRoleType, 0);
                break;
            case USR:
                setTabFragment(profileFragment);
                break;
        }
        if (rxBus.hasObservers()) {
            rxBus.send(new Events.PermissionEvent());
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == PERMISSION_CALLBACK_CONSTANT) {
            if (UtilityClass.hasPermissions(this, UtilityClass.PERMISSIONS)) {
                proceedAfterPermission();
            } else {
                Toast.makeText(DashboardActivity.this, "Oops you just denied the permission", Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void formatScanData(String[] QRCode, int status, String optionalMessage) throws JSONException, NumberFormatException {
        JSONObject object = new JSONObject();
        JSONArray uploadDataList = new JSONArray();

        JSONObject uploadObject = new JSONObject().put("userid", Integer.parseInt(QRCode[1]))
                .put("fromuserid", sharedPrefsHelper.get(PREF_USER_ID, 0))
                .put("status", status)
                .put("message", optionalMessage)
                .put("timestamp", String.valueOf(System.currentTimeMillis()));
        object.put("objectItems", uploadDataList.put(uploadObject));

        Log.e("TAG", object.toString());
        mPresenter.sendScanData(object.toString(), false);
    }

    @Override
    public void showMessage(String errorMessage) {
        Toast.makeText(DashboardActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void formatForLocalStorage(String scannedRequest) {
        JSONObject object = null;
        try {
            object = new JSONObject(scannedRequest).getJSONArray("objectItems").getJSONObject(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.e("FAILED LOG", object.toString());
        String data = object.toString().replaceAll("'", "''").replaceAll("\"", "\\\"");
        if (mPresenter.saveDataLocally(data)) {
            Log.e("Failed entry", " Success ==>Information was saved.");
        } else {
            Log.e("Failed entry", " Failure ==>Unable to save information.");
        }
    }

    @Override
    public void showVersionPopup(String isValidVersion) {
        if (isValidVersion.equalsIgnoreCase("false")) {
            ForceUpdateDialog dialog = new ForceUpdateDialog(DashboardActivity.this, true,
                    "https://play.google.com/store/apps/details?id=in.goaemex.www", false);
            dialog.setCanceledOnTouchOutside(false);
            dialog.setCancelable(false);
            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            dialog.show();
        }

    }

    @Override
    public void showProgressLoader(boolean toShow) {
        if (toShow) {
            CustomProgressDialog.showProgressDialog(this, null);
        } else {
            CustomProgressDialog.hideProgressDialog();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //Export-CSV feature
       /* getMenuInflater().inflate(R.menu.export_csv, menu);
        MenuItem exportCSV = menu.findItem(R.id.export_csv);
        exportCSV.setVisible(showCSV && mRoleType == UtilityClass.UserRole.REC);*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == R.id.export_csv) {
            File path = getExternalStoragePublicDirectory("Documents");
            if (!path.exists())
                path.mkdir();
            File file = new File(path, "ScannedLog.csv");
            mPresenter.exportScannedLog(file, String.valueOf(sharedPrefsHelper.get(PREF_USER_ID, 0)));

        }
        return super.onOptionsItemSelected(item);
    }
}