package in.goaemex.www.dashboard.edit_profile;

/**
 * Created by Vaibhav Barad on 5/28/2018.
 */
public class EditProfileModel {
    String status;
    String message;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
