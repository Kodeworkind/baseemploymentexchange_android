package in.goaemex.www.dashboard;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import javax.inject.Inject;

import in.goaemex.www.dashboard.qr_scan.QRUploadModel;
import in.goaemex.www.dashboard.scan_log.LogModel;
import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public class DashboardActivityPresenter implements DashboardActivityContract.Presenter {

    private DashboardActivityContract.View mView;
    private DataSource mDataSource;

    private CompositeDisposable compositeDisposable;
    private Disposable userDisposable;

    @Inject
    public DashboardActivityPresenter(DashboardActivityContract.View view, Repository mDataSource, CompositeDisposable compositeDisposable) {
        this.mDataSource = mDataSource;
        this.mView = view;
        this.compositeDisposable = compositeDisposable;
    }

    @Override
    public void takeView(DashboardActivityContract.View view) {

    }

    @Override
    public void dropView() {
        mView = null;
    }

    @Override
    public void validateQRScan(String QRData) {
        String[] splitData;
        if (QRData != null && !QRData.isEmpty()) {
            splitData = QRData.split("-");
            if (splitData.length == 2 && splitData[0].equals("goaempex")) {
                try {
                    mView.formatScanData(splitData, 0, "");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                mView.showMessage("The QR code doesn't belong to this event");
            }
        }
    }

    @Override
    public void sendScanData(String scannedRequest, boolean isFailSync) {
        DisposableSingleObserver<QRUploadModel> disposableSingleObserver = new DisposableSingleObserver<QRUploadModel>() {
            @Override
            public void onSuccess(QRUploadModel qrUploadModel) {
                if (qrUploadModel != null && qrUploadModel.getStatuslist().size() > 0) {
                    //mView.showMessage("Upload Successful");
                    mDataSource.checkUpdateDatabase(qrUploadModel);
                }
            }

            @Override
            public void onError(Throwable e) {
                mView.formatForLocalStorage(scannedRequest);
            }
        };

        if (!compositeDisposable.isDisposed()) {
            Single<QRUploadModel> mUserSingle = mDataSource.sendScanData(scannedRequest);
            userDisposable = mUserSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(disposableSingleObserver);
            compositeDisposable.add(disposableSingleObserver);
            compositeDisposable.add(userDisposable);
        }
    }

    @Override
    public void syncFailedData() {
        try {
            String failedData[];
            String failedEntry = mDataSource.getFailedData();
            JSONObject object = new JSONObject();
            JSONArray uploadDataList = new JSONArray();

            if (failedEntry != null && !failedEntry.isEmpty()) {
                failedData = failedEntry.replaceAll("''", "'").replaceAll("\\\\\"", "\"").split("&");
                for (String data : failedData) {
                    String temp[] = data.split("_");
                    JSONObject object1 = new JSONObject(temp[1]).put("localid", temp[0]);

                    uploadDataList.put(object1);
                }
                object.put("objectItems", uploadDataList);
                sendScanData(object.toString(), false);
            } else {
                Log.e("Sync status", "No data to sync");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean saveDataLocally(String data) {
        return mDataSource.saveFailedEntry(data);
    }

    @Override
    public void checkVersionValidity(int versionCode) {
        DisposableSingleObserver<VersionValidity> disposableSingleObserver = new DisposableSingleObserver<VersionValidity>() {
            @Override
            public void onSuccess(VersionValidity validity) {
                if (validity != null)
                    mView.showVersionPopup(validity.isValid());
            }

            @Override
            public void onError(Throwable e) {
                checkVersionValidity(versionCode);
            }
        };

        if (!compositeDisposable.isDisposed()) {
            Single<VersionValidity> mUserSingle = mDataSource.checkVersionValidity(versionCode);
            userDisposable = mUserSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(disposableSingleObserver);
            compositeDisposable.add(disposableSingleObserver);
            compositeDisposable.add(userDisposable);
        }
    }

    @Override
    public void exportScannedLog(File file, String userID) {
        mView.showProgressLoader(true);
        DisposableSingleObserver<LogModel> mSingleObserver = new DisposableSingleObserver<LogModel>() {
            @Override
            public void onSuccess(LogModel logModel) {
                FileWriter writer = null;
                try {
                    writer = new FileWriter(file);
                    writer.append("Name,Mobile,Email,Description,Status\n");
                    for (LogModel.LogData data :
                            logModel.getVisit()) {
                        writer.append(data.toString());
                        writer.append("\n");
                    }
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }


                mView.showProgressLoader(false);
            }

            @Override
            public void onError(Throwable e) {
                mView.showProgressLoader(false);
            }
        };
        if (!compositeDisposable.isDisposed()) {
            Single<LogModel> mLogSingle = mDataSource.getScanLog(userID);
            userDisposable = mLogSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(mSingleObserver);
            compositeDisposable.add(mSingleObserver);
            compositeDisposable.add(userDisposable);
        }
    }
}
