package in.goaemex.www.dashboard;

import dagger.Binds;
import dagger.Module;
import in.goaemex.www.di.ActivityScoped;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@Module
public abstract class DashboardActivityModule {

    @ActivityScoped
    @Binds
    abstract DashboardActivityContract.Presenter presenter(DashboardActivityPresenter presenter);

    @ActivityScoped
    @Binds
    abstract DashboardActivityContract.View view(DashboardActivity activity);
}
