package in.goaemex.www.dashboard.profile;

import dagger.Binds;
import dagger.Module;
import dagger.android.ContributesAndroidInjector;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.di.FragmentScoped;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@Module
public abstract class ProfileModule {

    @FragmentScoped
    @ContributesAndroidInjector
    abstract ProfileFragment profileFragment();

    @Binds
    @ActivityScoped
    abstract ProfileContract.Presenter presenter(ProfilePresenter profilePresenter);


}
