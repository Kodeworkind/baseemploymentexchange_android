package in.goaemex.www.dashboard.employer_attendance;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import java.util.ArrayList;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import in.goaemex.www.R;
import in.goaemex.www.data.local.SharedPrefsHelper;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.utility.CustomProgressDialog;

import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_ID;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@ActivityScoped
public class EmployerAttendanceFragment extends DaggerFragment implements EmployerAttendanceContract.View {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }


    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    @BindView(R.id.viewFlipper)
    ViewFlipper viewFlipper;
    Unbinder unbinder;
    @Inject
    EmployerAttendanceContract.Presenter mPresenter;
    @Inject
    SharedPrefsHelper mPrefsHelper;
    @BindView(R.id.errorMessage)
    TextView errorMessage;

    @Inject
    public EmployerAttendanceFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.scan_log_list, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter.takeView(this);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        recyclerView.setAdapter(new EmployerAttendanceLogAdapter(getActivity(), new ArrayList<>()));
        viewFlipper.setDisplayedChild(0);
        mPresenter.getEmployerAttendanceLog(String.valueOf(mPrefsHelper.get(PREF_USER_ID, 0)), 0);
        errorMessage.setText("Looks like we are having some problems");
        return view;
    }

    @Override
    public void onEmployerAttendanceData(EmployerAttendanceModel data) {
        if (data != null && data.getCompanyDetails().size() > 0) {
            viewFlipper.setDisplayedChild(1);
            ((EmployerAttendanceLogAdapter) recyclerView.getAdapter()).onDataUpdate(data.getCompanyDetails());
        } else {
            viewFlipper.setDisplayedChild(2);
        }
    }

    @Override
    public void employerAttendanceLogFailure(String message) {
        viewFlipper.setDisplayedChild(2);
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void showProgressLoader(boolean toShow) {
        if (toShow) {
            CustomProgressDialog.showProgressDialog(getActivity(), null);
        } else {
            CustomProgressDialog.hideProgressDialog();
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.dropView();

    }
}
