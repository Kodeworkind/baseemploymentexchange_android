package in.goaemex.www.dashboard.profile;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatDelegate;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import dagger.android.support.DaggerFragment;
import in.goaemex.www.R;
import in.goaemex.www.data.Events;
import in.goaemex.www.data.RxBus;
import in.goaemex.www.data.local.SharedPrefsHelper;
import in.goaemex.www.di.ActivityScoped;
import in.goaemex.www.utility.CustomProgressDialog;

import static in.goaemex.www.data.local.SharedPrefsHelper.PREF_USER_EMAIL;
import static in.goaemex.www.utility.ObjectUtil.isNull;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
@ActivityScoped
public class ProfileFragment extends DaggerFragment implements ProfileContract.View {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }



    @BindView(R.id.iv_qr)
    ImageView ivQr;
    @BindView(R.id.iv_edit)
    ImageView ivEdit;
    @BindView(R.id.tv_name_value)
    TextView tvNameValue;
    @BindView(R.id.tv_mName_value)
    TextView tvMNameValue;
    @BindView(R.id.tv_lName_value)
    TextView tvLNameValue;
    @BindView(R.id.tv_address_value)
    TextView tvAddressValue;
    @BindView(R.id.tv_mobile_value)
    TextView tvMobileValue;
    @BindView(R.id.tv_dob_value)
    TextView tvDobValue;
    @BindView(R.id.tv_email_value)
    TextView tvEmailValue;
    @BindView(R.id.tv_employment_no_value)
    TextView tvEmploymentNoValue;
    @BindView(R.id.tv_qualification_value)
    TextView tvQualificationValue;
    @BindView(R.id.tv_employment_status_value)
    TextView tvEmploymentStatusValue;
    @BindView(R.id.tv_experience_value)
    TextView tvExperienceValue;
    @BindView(R.id.tv_experience)
    TextView tvExperience;
    Unbinder unbinder;

    @Inject
    ProfileContract.Presenter mPresenter;
    @Inject
    SharedPrefsHelper mSharedPrefsHelper;
    @Inject
    RxBus rxBus;
    private ProfileModel mProfileData;


    @Inject
    public ProfileFragment() {
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_fragment, container, false);
        unbinder = ButterKnife.bind(this, view);
        mPresenter.takeView(this);
        mPresenter.getProfileData(mSharedPrefsHelper.get(PREF_USER_EMAIL, ""));

        ivQr.setTag(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                ivQr.setVisibility(View.VISIBLE);
                ivQr.setImageBitmap(bitmap);
                ivQr.setScaleType(ImageView.ScaleType.CENTER_CROP);
            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {
                ivQr.setVisibility(View.GONE);
            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
        return view;
    }

    @Override
    public void showProgressLoader(boolean toShow) {
        if(toShow){
            CustomProgressDialog.showProgressDialog(getActivity(),null);
        }else{
            CustomProgressDialog.hideProgressDialog();
        }
    }


    @Override
    public void onResume() {
        super.onResume();
        mPresenter.takeView(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        mPresenter.dropView();
    }

    @OnClick(R.id.iv_edit)
    public void onViewClicked() {
        if (rxBus.hasObservers()) {
            rxBus.send(new Events.EditProfileEvent(mProfileData, tvNameValue, tvMNameValue, tvLNameValue,
                    tvAddressValue, tvMobileValue, tvEmploymentNoValue,
                    tvQualificationValue, tvEmploymentStatusValue, tvExperienceValue));
        }
    }

    @Override
    public void onUserData(ProfileModel profileData) {
        mProfileData = profileData;
        if (!isNull(profileData.getUserDetails().getQRImage()) && !profileData.getUserDetails().getQRImage().isEmpty()) {
            Picasso.get()
                    .load(profileData.getUserDetails().getQRImage())
                    .into((Target) ivQr.getTag());
        } else {
            ivQr.setVisibility(View.GONE);
        }

        tvNameValue.setText(profileData.getUserDetails().getName());
        tvLNameValue.setText(profileData.getUserDetails().getSurname());
        tvMNameValue.setText(profileData.getUserDetails().getMiddleName());
        tvAddressValue.setText(profileData.getUserDetails().getAddress());
        tvMobileValue.setText(profileData.getUserDetails().getMobileNo());
        tvDobValue.setText(profileData.getUserDetails().getDateOfBirth());
        tvEmailValue.setText(profileData.getUserDetails().getEmailId());
        tvEmploymentNoValue.setText(profileData.getUserDetails().getEmploymentCardNo());
        tvQualificationValue.setText(profileData.getUserDetails().getQualification());
        tvEmploymentStatusValue.setText(profileData.getUserDetails().getEmploymentStatus());

        if (profileData.getUserDetails().getEmploymentStatus().equalsIgnoreCase("employed")) {
            tvExperienceValue.setVisibility(View.VISIBLE);
            tvExperience.setVisibility(View.VISIBLE);
            tvExperienceValue.setText(profileData.getUserDetails().getYearsOfExperience());
        } else {
            tvExperienceValue.setVisibility(View.GONE);
            tvExperience.setVisibility(View.GONE);
        }
    }

    @Override
    public void UserDataFailure(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 99 && resultCode == Activity.RESULT_OK) {
            if (data != null && data.getParcelableExtra("user_data") != null) {
                onUserData(data.getParcelableExtra("user_data"));
            }
        }
    }
}