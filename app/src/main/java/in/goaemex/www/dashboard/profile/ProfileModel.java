package in.goaemex.www.dashboard.profile;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Vaibhav Barad on 5/26/2018.
 */
public class ProfileModel implements Parcelable {

    @SerializedName("status")
    @Expose
    public String status;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("UserDetails")
    @Expose
    public UserDetails userDetails;

    protected ProfileModel(Parcel in) {
        status = in.readString();
        message = in.readString();
        userDetails = in.readParcelable(UserDetails.class.getClassLoader());
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(status);
        dest.writeString(message);
        dest.writeParcelable(userDetails, flags);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<ProfileModel> CREATOR = new Creator<ProfileModel>() {
        @Override
        public ProfileModel createFromParcel(Parcel in) {
            return new ProfileModel(in);
        }

        @Override
        public ProfileModel[] newArray(int size) {
            return new ProfileModel[size];
        }
    };

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserDetails getUserDetails() {
        return userDetails;
    }

    public void setUserDetails(UserDetails userDetails) {
        this.userDetails = userDetails;
    }

    public static class UserDetails implements Parcelable {

        @SerializedName("UserID")
        @Expose
        public int userID;
        @SerializedName("Name")
        @Expose
        public String name;
        @SerializedName("MiddleName")
        @Expose
        public String middleName;
        @SerializedName("Surname")
        @Expose
        public String surname;
        @SerializedName("Gender")
        @Expose
        public String gender;
        @SerializedName("Address")
        @Expose
        public String address;
        @SerializedName("Taluka")
        @Expose
        public String taluka;
        @SerializedName("State")
        @Expose
        public String state;
        @SerializedName("MobileNo")
        @Expose
        public String mobileNo;
        @SerializedName("EmailId")
        @Expose
        public String emailId;
        @SerializedName("EmploymentCardNo")
        @Expose
        public String employmentCardNo;
        @SerializedName("DateOfBirth")
        @Expose
        public String dateOfBirth;
        @SerializedName("Qualification")
        @Expose
        public String qualification;
        @SerializedName("EmploymentStatus")
        @Expose
        public String employmentStatus;
        @SerializedName("YearsOfExperience")
        @Expose
        public String yearsOfExperience;
        @SerializedName("UserCode")
        @Expose
        public String userCode;
        public String QRImage;

        public int getUserID() {
            return userID;
        }

        public void setUserID(int userID) {
            this.userID = userID;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMiddleName() {
            return middleName;
        }

        public void setMiddleName(String middleName) {
            this.middleName = middleName;
        }

        public String getSurname() {
            return surname;
        }

        public void setSurname(String surname) {
            this.surname = surname;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getTaluka() {
            return taluka;
        }

        public void setTaluka(String taluka) {
            this.taluka = taluka;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getMobileNo() {
            return mobileNo;
        }

        public void setMobileNo(String mobileNo) {
            this.mobileNo = mobileNo;
        }

        public String getEmailId() {
            return emailId;
        }

        public void setEmailId(String emailId) {
            this.emailId = emailId;
        }

        public String getEmploymentCardNo() {
            return employmentCardNo;
        }

        public void setEmploymentCardNo(String employmentCardNo) {
            this.employmentCardNo = employmentCardNo;
        }

        public String getDateOfBirth() {
            return dateOfBirth;
        }

        public void setDateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
        }

        public String getQualification() {
            return qualification;
        }

        public void setQualification(String qualification) {
            this.qualification = qualification;
        }

        public String getEmploymentStatus() {
            return employmentStatus;
        }

        public void setEmploymentStatus(String employmentStatus) {
            this.employmentStatus = employmentStatus;
        }

        public String getYearsOfExperience() {
            return yearsOfExperience;
        }

        public void setYearsOfExperience(String yearsOfExperience) {
            this.yearsOfExperience = yearsOfExperience;
        }

        public String getUserCode() {
            return userCode;
        }

        public void setUserCode(String userCode) {
            this.userCode = userCode;
        }

        public String getQRImage() {
            return QRImage;
        }

        public void setQRImage(String QRImage) {
            this.QRImage = QRImage;
        }

        protected UserDetails(Parcel in) {
            userID = in.readInt();
            name = in.readString();
            middleName = in.readString();
            surname = in.readString();
            gender = in.readString();
            address = in.readString();
            taluka = in.readString();
            state = in.readString();
            mobileNo = in.readString();
            emailId = in.readString();
            employmentCardNo = in.readString();
            dateOfBirth = in.readString();
            qualification = in.readString();
            employmentStatus = in.readString();
            yearsOfExperience = in.readString();
            userCode = in.readString();
            QRImage = in.readString();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userID);
            dest.writeString(name);
            dest.writeString(middleName);
            dest.writeString(surname);
            dest.writeString(gender);
            dest.writeString(address);
            dest.writeString(taluka);
            dest.writeString(state);
            dest.writeString(mobileNo);
            dest.writeString(emailId);
            dest.writeString(employmentCardNo);
            dest.writeString(dateOfBirth);
            dest.writeString(qualification);
            dest.writeString(employmentStatus);
            dest.writeString(yearsOfExperience);
            dest.writeString(userCode);
            dest.writeString(QRImage);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public static final Creator<UserDetails> CREATOR = new Creator<UserDetails>() {
            @Override
            public UserDetails createFromParcel(Parcel in) {
                return new UserDetails(in);
            }

            @Override
            public UserDetails[] newArray(int size) {
                return new UserDetails[size];
            }
        };
    }

}


