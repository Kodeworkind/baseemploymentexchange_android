package in.goaemex.www.dashboard.scan_log;

import javax.inject.Inject;

import in.goaemex.www.data.DataSource;
import in.goaemex.www.data.Repository;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import static in.goaemex.www.utility.ObjectUtil.isNull;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public class ScanLogPresenter implements ScanLogContract.Presenter {

    private ScanLogContract.View mView;
    private DataSource mDataSource;
    private Disposable disposable;
    private CompositeDisposable mCompositeDisposable;


    @Inject
    public ScanLogPresenter(Repository mDataSource, CompositeDisposable mCompositeDisposable) {
        this.mDataSource = mDataSource;
        this.mCompositeDisposable = mCompositeDisposable;
    }

    @Override
    public void getScanLog(String userID) {
        mView.showProgressLoader(true);
        DisposableSingleObserver<LogModel> mSingleObserver = new DisposableSingleObserver<LogModel>() {
            @Override
            public void onSuccess(LogModel logModel) {
                if (!isNull(logModel))
                    mView.onScanLogs(logModel);
                else
                    mView.scanLogFailure("Sorry! Looks like there is no scan history");
                mView.showProgressLoader(false);
            }

            @Override
            public void onError(Throwable e) {
                mView.showProgressLoader(false);
                mView.scanLogFailure("Sorry! Looks like there is no scan history");
            }
        };
        if (!mCompositeDisposable.isDisposed()) {
            Single<LogModel> mLogSingle = mDataSource.getScanLog(userID);
            disposable = mLogSingle.subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread()).subscribeWith(mSingleObserver);
            mCompositeDisposable.add(mSingleObserver);
            mCompositeDisposable.add(disposable);
        }

    }

    @Override
    public void takeView(ScanLogContract.View view) {
        mView = view;
    }

    @Override
    public void dropView() {
        mView = null;
        if (mCompositeDisposable != null && !mCompositeDisposable.isDisposed()) {
            mCompositeDisposable.remove(disposable);
            mCompositeDisposable.clear();
        }
    }
}