package in.goaemex.www.dashboard.profile;

import in.goaemex.www.base_class.BasePresenter;
import in.goaemex.www.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public interface ProfileContract {

    interface View extends BaseView<Presenter>{

        void onUserData(ProfileModel profileData);

        void UserDataFailure(String message);
    }

    interface Presenter extends BasePresenter<View>{

        void getProfileData(String userEmail);
    }
}

