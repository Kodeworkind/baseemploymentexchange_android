package in.goaemex.www.dashboard;

import org.json.JSONException;

import java.io.File;

import in.goaemex.www.base_class.BasePresenter;
import in.goaemex.www.base_class.BaseView;

/**
 * Created by Vaibhav Barad on 5/25/2018.
 */
public interface DashboardActivityContract {
    interface View extends BaseView<Presenter> {
        void formatScanData(String[] QRCode,  int status, String optionalMessage) throws JSONException;

        void showMessage(String message);

        void formatForLocalStorage(String scannedRequest);

        void showVersionPopup(String isValidVersion);
    }

    interface Presenter extends BasePresenter<View> {
        void validateQRScan(String QRData);

        void sendScanData(String scannedRequest,boolean isFailSync);

        void syncFailedData();

        boolean saveDataLocally(String data);

        void checkVersionValidity(int versionCode);

        void exportScannedLog(File file, String userID);
    }
}

