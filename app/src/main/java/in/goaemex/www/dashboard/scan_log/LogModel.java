package in.goaemex.www.dashboard.scan_log;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import in.goaemex.www.utility.UtilityClass;

/**
 * Created by Vaibhav Barad on 1/29/2018.
 */

public class LogModel implements Parcelable {
    public static final Creator<LogModel> CREATOR = new Creator<LogModel>() {
        @Override
        public LogModel createFromParcel(Parcel in) {
            return new LogModel(in);
        }

        @Override
        public LogModel[] newArray(int size) {
            return new LogModel[size];
        }
    };
    @SerializedName("scan_log")
    @Expose
    private ArrayList<LogData> visit = null;

    public LogModel() {
    }

    protected LogModel(Parcel in) {
        visit = in.createTypedArrayList(LogData.CREATOR);
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeTypedList(visit);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public ArrayList<LogData> getVisit() {
        return visit;
    }

    public void setVisit(ArrayList<LogData> visit) {
        this.visit = visit;
    }

    public static class LogData implements Parcelable {

        public static final Creator<LogData> CREATOR = new Creator<LogData>() {
            @Override
            public LogData createFromParcel(Parcel in) {
                return new LogData(in);
            }

            @Override
            public LogData[] newArray(int size) {
                return new LogData[size];
            }
        };
        @SerializedName("userid")
        @Expose

        private int userid;
        @SerializedName("name")
        @Expose

        private String name;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("type")
        @Expose
        private String type;

        @SerializedName("email")
        @Expose
        private String email;

        @SerializedName("status")
        @Expose
        private int status;

        @SerializedName("message")
        @Expose
        private String shortText;
        @SerializedName("session")
        @Expose
        private String session;

        public LogData() {
        }

        protected LogData(Parcel in) {
            userid = in.readInt();
            name = in.readString();
            mobile = in.readString();
            type = in.readString();
            email = in.readString();
            shortText = in.readString();
            session = in.readString();
            status = in.readInt();
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            dest.writeInt(userid);
            dest.writeString(name);
            dest.writeString(mobile);
            dest.writeString(type);
            dest.writeString(email);
            dest.writeString(shortText);
            dest.writeString(session);
            dest.writeInt(status);
        }

        @Override
        public int describeContents() {
            return 0;
        }

        public int getUserid() {
            return userid;
        }

        public void setUserid(int userid) {
            this.userid = userid;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getType() {
            return type;
        }

        public void setType(String type) {
            this.type = type;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public int getStatus() {
            return status;
        }

        public void setStatus(int status) {
            this.status = status;
        }

        public String getShortText() {
            return shortText;
        }

        public void setShortText(String shortText) {
            this.shortText = shortText;
        }

        public String getSession() {
            return session;
        }

        public void setSession(String session) {
            this.session = session;
        }

        @Override
        public String toString() {
            return UtilityClass.escapeString(name) + "," +  UtilityClass.escapeString(mobile)+ "," +
                    UtilityClass.escapeString(email ) + ","+  UtilityClass.escapeString(shortText) +
                    "," + UtilityClass.getInterviewStatus(status);
        }
    }
}
