package in.goaemex.www.recruiter_option;

import android.content.Intent;
import android.os.Bundle;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import dagger.android.support.DaggerAppCompatActivity;
import in.goaemex.www.R;
import in.goaemex.www.data.RxBus;
import in.goaemex.www.data.local.SharedPrefsHelper;
import in.goaemex.www.di.ActivityScoped;

/**
 * Created by Vaibhav Barad on 5/27/2018.
 */
@ActivityScoped
public class RecruiterOptionActivity extends DaggerAppCompatActivity {
    static {
        AppCompatDelegate.setCompatVectorFromResourcesEnabled(true);
    }

    String QRData = null;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.not_interested)
    TextView notInterested;
    @BindView(R.id.shortlisted)
    TextView shortlisted;
    @BindView(R.id.hired)
    TextView hired;

    String[] splitData;
    @BindView(R.id.shortText)
    EditText shortText;
    @BindView(R.id.recruiterOptional)
    ConstraintLayout recruiterOptional;
    @BindView(R.id.detail)
    ConstraintLayout detail;

    boolean isQRValid;

    @BindView(R.id.coordinatorLayout)
    CoordinatorLayout coordinatorLayout;
    Snackbar snackbar;

    @Inject
    SharedPrefsHelper sharedPrefsHelper;
    @Inject
    RxBus rxBus;
    @BindView(R.id.scrutiny)
    TextView scrutiny;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recruiter_option);
        ButterKnife.bind(this);
        setupToolbar();

        QRData = getIntent().getStringExtra("qrCode");
        splitData = QRData.split("-");

        shortText.setMaxLines(4);
        shortText.setVerticalScrollBarEnabled(true);
        shortText.setMovementMethod(new ScrollingMovementMethod());
    }

    private void setupToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        toolbar.setTitleTextColor(ContextCompat.getColor(this, R.color.white));
        getSupportActionBar().setTitle("");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            finish();
        return super.onOptionsItemSelected(item);
    }

    @OnClick({R.id.not_interested, R.id.shortlisted, R.id.hired,R.id.scrutiny})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.not_interested:
                closeActivity(QRData, 3, shortText.getText().toString().trim());
                break;
            case R.id.shortlisted:
                closeActivity(QRData, 1, shortText.getText().toString().trim());
                break;
            case R.id.hired:
                closeActivity(QRData, 2, shortText.getText().toString().trim());
                break;
            case R.id.scrutiny:
                closeActivity(QRData, 4, shortText.getText().toString().trim());
                break;
        }
    }

    void closeActivity(String qrData, int status, String optionalMessage) {
        setResult(RESULT_OK, new Intent().putExtra("status", status).putExtra("optionalMessage", optionalMessage).putExtra("qrData", qrData));
        finish();
    }
}
